/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leader;

import javax.swing.JFrame;

/**
 *
 * @author tanng
 */
public class ClassSaveDataWhenClickOnRow {
    private static ClassSaveDataWhenClickOnRow instance;
    private ClassSaveDataWhenClickOnRow(){
    }
    public static ClassSaveDataWhenClickOnRow getInstance() {
        if (instance == null) {
            instance = new ClassSaveDataWhenClickOnRow();
        }
        return instance;
    }

    public String getTaskID() {
        return taskID;
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }
    

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectName) {
        this.projectID = projectName;
    }
    
    public void saveData(String projectID,String projectName,String taskID/*,JFrame ftl*/){
        this.projectID = projectID;
        this.taskID = taskID;
        this.projectName = projectName;
//        this.f = ftl;
//        f.setEnabled(false);
    }
    String taskID = "";
    String projectID = "";
    String projectName = "";

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    JFrame f;

//    public FormTaskList getF() {
//        return f;
//    }
//
//    public void setF(FormTaskList f) {
//        this.f = f;
//    }

    public JFrame getF() {
        return f;
    }

    public void setF(JFrame f) {
        this.f = f;
    }
}
