/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leader;

import com.microsoft.sqlserver.jdbc.StringUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import main.ChucNang;
import main.Design;
import main.FormMain;
import main.NhanVien;

/**
 *
 * @author tanng
 */
public class FormTaskList extends javax.swing.JFrame {

    //<editor-fold defaultstate="collapsed" desc=" Khởi Tạo Model Table ">
    public DefaultTableModel model = null;
    public DefaultTableModel getModel(){
        return model = (DefaultTableModel)tbTask.getModel();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Khởi Tạo Model combobox ">
    public DefaultComboBoxModel comboBoxModel = null;
    public DefaultComboBoxModel getComboBoxModel(){
        return comboBoxModel = (DefaultComboBoxModel)cboProject.getModel();
    }
    //</editor-fold>
    private final static int DELETE_TASK = 0;
    private final static int FINISH_TASK = 1;
    private final static int CANCEL_TASK = 2;
    Icon icon1 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/checkmark-50.png"));
    Icon icon2 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/error-50.png"));
    /**
     * Creates new form FormTaskList
     */
    public FormTaskList() throws SQLException {
        initComponents();
        getModel();
        getComboBoxModel();
        ChucNang cn = new ChucNang();
        tbTask.setDefaultEditor(Object.class , null); // không được phép sửa trên jtable
        tbTask.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // chỉ được phép chọn 1 dòng trong Jtable
        cn.showComboboxProject(comboBoxModel); // show danh sách dự án vào combobox
        showTaskWithComboxProjectFormLeader(); // show công việc tại combobox dự án được chọn
        hideButton();
        design();
        
    }
    private class WindowEventHandler extends WindowAdapter {
                public void windowClosing(WindowEvent evt) {                  
                        FormMain fm = new FormMain();
                        fm.show();
                        dispose();      
                        
  }
}
    private void design(){
        // <editor-fold defaultstate="collapsed" desc="Design"> 
        //=============== Closebutton =======================
        this.addWindowListener(new WindowEventHandler());
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        
         //============== bảng màu =========================
        /*   #feffde = 254, 255, 222 = kem sữa.
              #ddffbc =  221, 255, 188 = xanh nhạt 1.
              #91c788 = 145, 199, 136 = xanh đậm 1 .
              #52734d = 82, 115, 77 = xanh đậm 2.  */
        Color tbbackground = new Color(254, 255, 222);
        Color tbforeground = new Color(0,0,0);
        Color tbselectionbackground = new Color(145, 199, 136);
        Color tbselectionforeground = new Color(255,0,0);
         //============== header Input =======================
        JTableHeader header = tbTask.getTableHeader();
        header.setForeground(Color.black);
        header.setBackground(Color.RED);
        header.setFont(new Font("Times New Roman", Font.BOLD , 20));
        tbTask.setFont(new Font("Times New Roman", Font.PLAIN , 20));
        
        tbTask.setBackground(tbbackground);
        tbTask.setForeground(tbforeground);
        tbTask.setSelectionBackground(tbselectionbackground);
        tbTask.setSelectionForeground(tbselectionforeground);
        this.pack();
         //============== Set JTable =========================
        new Design().resizeHeaderLong(tbTask);
        tbTask.getColumnModel().getColumn(0).setCellRenderer(new Design.AlignRenderer());
        tbTask.getColumnModel().getColumn(2).setCellRenderer(new Design.AlignRenderer());
        tbTask.getColumnModel().getColumn(3).setCellRenderer(new Design.AlignRenderer());
        tbTask.getColumnModel().getColumn(4).setCellRenderer(new Design.AlignRenderer());
        tbTask.getColumnModel().getColumn(5).setCellRenderer(new Design.AlignRenderer());
            
        tbTask.getColumnModel().getColumn(1).setPreferredWidth(200);
        tbTask.getColumnModel().getColumn(2).setPreferredWidth(170);
        tbTask.getColumnModel().getColumn(3).setPreferredWidth(172);
        // </editor-fold>
    }

    private void showTaskWithComboxProjectFormLeader() throws SQLException{  // show công việc tại combobox dự án được chọn
        try
        {
            getModel();
            ChucNang cn = new ChucNang();
            String projectName = cboProject.getSelectedItem().toString();
            cn.showTaskInFormLeader(model,projectName);
        } catch (Exception e)
        {
            //JOptionPane.showMessageDialog(null, "Hiện Tại Bạn Chưa Có Dự Án"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
        }
        
        
    }
    private void hideButton(){
        if(NhanVien.getInstance().getMachucvu() == 1 || NhanVien.getInstance().getMachucvu() == 2){
            btnTaskAssigning.setVisible(false);
            btnTaskDeleting.setVisible(false);
            btnTaskCompleting.setVisible(false);
            btnCancelReport.setVisible(false);
        }
    }
    private boolean checkTask(int command){ // nếu trả về true thì sẽ update Task
        getModel();
        int a = tbTask.getSelectedRow();
        String staffName = model.getValueAt(a, 1).toString();
        String statusTask = model.getValueAt(a, 4).toString();
        String statusReport = model.getValueAt(a, 5).toString();
        if(command == 1){
            if(statusTask.equals("Đã Xóa")) {
                JOptionPane.showMessageDialog(null, "Công Việc Này Đã Bị Xóa"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
                return false;
            }
            else if(staffName.equals("")) {
                JOptionPane.showMessageDialog(null, "Vẫn Chưa Có Nhân Viên Cho Công Việc Này"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
                return false;
            }else if(txtTaskReport.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Nhân Viên Chưa Gửi Báo Cáo Cho Công Việc Này"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
                return false;
            }else if(statusTask.equals("Hoàn Thành")){
                JOptionPane.showMessageDialog(null, "Công Việc Này Đã Được Hoàn Thành"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
                return false;
            }
        }else if(command == 0){
            if(statusTask.equals("Đã Xóa"))  {
                JOptionPane.showMessageDialog(null, "Công Việc Này Đã Bị Xóa"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
                return false;
            }
            else if(statusTask.equals("Hoàn Thành")){
                JOptionPane.showMessageDialog(null, "Không Thể Xóa Công Việc Đã Hoàn Thành"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
                return false;
            }
        }else if(command == 2){
            if(statusTask.equals("Hoàn Thành")){
                JOptionPane.showMessageDialog(null, "Không Thể Hủy Bỏ Công Việc Đã Hoàn Thành"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
                return false;
            }else if(statusTask.equals("Đã Xóa"))  {
                JOptionPane.showMessageDialog(null, "Công Việc Này Đã Bị Xóa"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
                return false;
            }else if(statusReport.equals("Chưa Có Báo Cáo"))  {
                JOptionPane.showMessageDialog(null, "Công Việc Này Chưa Có Báo Cáo"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
                return false;
            }
        }
        return true;
    }
    
    private void updateTask(int command){
        try
        {
            if(checkTask(command)){
                ChucNang cn = new ChucNang();
                getModel();
                int a = tbTask.getSelectedRow();
                String taskID = model.getValueAt(a, 0).toString();
                String projectName = cboProject.getSelectedItem().toString();
                cn.updateTask(projectName,taskID,command);
                showTaskWithComboxProjectFormLeader();
                if(command == 2) txtTaskReport.setText("");
            }
        } catch (SQLException ex)
        {
            Logger.getLogger(FormTaskList.class.getName()).log(Level.SEVERE, null, ex);
        }catch (Exception e){
            if(tbTask.getSelectedRow() == -1) 
            JOptionPane.showMessageDialog(null, "Bạn chưa chọn công việc"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
//            if(tbTask.getSelectedRow() == -1) MessageBox.showMessageBox().showError("bạn chưa chọn công việc");
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        cboProject = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbTask = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtTaskReport = new javax.swing.JTextArea();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtTaskDescription = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btnTaskAssigning = new javax.swing.JButton();
        btnTaskCompleting = new javax.swing.JButton();
        btnTaskDeleting = new javax.swing.JButton();
        btnCancelReport = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        btnExit = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Danh Sách Công Việc");
        setLocationByPlatform(true);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 255, 204));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("Danh Sách Công Việc");

        jPanel3.setBackground(new java.awt.Color(204, 255, 204));

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));

        cboProject.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        cboProject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboProjectActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setText("Chọn Dự Án");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(cboProject, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cboProject, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel2))
        );

        tbTask.setBackground(new java.awt.Color(254, 255, 222));
        tbTask.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã Công Việc", "Tên Nhân Viên", "Ngày Bắt Đầu", "Ngày Kết Thúc", "Trạng Thái Công Việc", "Trạng Thái Báo Cáo"
            }
        ));
        tbTask.setFillsViewportHeight(true);
        tbTask.getTableHeader().setReorderingAllowed(false);
        tbTask.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbTaskMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tbTask);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 1085, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4)
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(204, 255, 204));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 0, 255));
        jLabel4.setText("Nội Dung Báo Cáo :");

        txtTaskReport.setEditable(false);
        txtTaskReport.setColumns(20);
        txtTaskReport.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        txtTaskReport.setRows(5);
        jScrollPane3.setViewportView(txtTaskReport);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 362, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(204, 255, 204));

        txtTaskDescription.setEditable(false);
        txtTaskDescription.setColumns(20);
        txtTaskDescription.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        txtTaskDescription.setRows(5);
        jScrollPane2.setViewportView(txtTaskDescription);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 0, 255));
        jLabel3.setText("Nội Dung Công Việc :");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(204, 255, 204));

        btnTaskAssigning.setBackground(new java.awt.Color(255, 255, 255));
        btnTaskAssigning.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnTaskAssigning.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/add-user-group-30.png"))); // NOI18N
        btnTaskAssigning.setText("Phân Công");
        btnTaskAssigning.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnTaskAssigning.setIconTextGap(15);
        btnTaskAssigning.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTaskAssigningActionPerformed(evt);
            }
        });

        btnTaskCompleting.setBackground(new java.awt.Color(255, 255, 255));
        btnTaskCompleting.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnTaskCompleting.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/task-completed-30.png"))); // NOI18N
        btnTaskCompleting.setText("Hoàn Thành");
        btnTaskCompleting.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnTaskCompleting.setIconTextGap(15);
        btnTaskCompleting.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTaskCompletingActionPerformed(evt);
            }
        });

        btnTaskDeleting.setBackground(new java.awt.Color(255, 255, 255));
        btnTaskDeleting.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnTaskDeleting.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/cancel-30.png"))); // NOI18N
        btnTaskDeleting.setText("Xóa");
        btnTaskDeleting.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnTaskDeleting.setIconTextGap(35);
        btnTaskDeleting.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTaskDeletingActionPerformed(evt);
            }
        });

        btnCancelReport.setBackground(new java.awt.Color(255, 255, 255));
        btnCancelReport.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnCancelReport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/scorecard-30.png"))); // NOI18N
        btnCancelReport.setText("Hủy Bỏ Báo Cáo");
        btnCancelReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelReportActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCancelReport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnTaskDeleting, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnTaskCompleting, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnTaskAssigning, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnTaskAssigning, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnTaskCompleting, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnTaskDeleting, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCancelReport, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(204, 255, 204));

        btnExit.setBackground(new java.awt.Color(255, 255, 255));
        btnExit.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/undo-30.png"))); // NOI18N
        btnExit.setText("Trở Lại");
        btnExit.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnExit)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(389, 389, 389)
                        .addComponent(jLabel1)))
                .addGap(0, 12, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(171, 171, 171)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        new FormMain().show();
        dispose();
    }//GEN-LAST:event_btnExitActionPerformed

    private void cboProjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboProjectActionPerformed
        try
        {
            showTaskWithComboxProjectFormLeader();
        } catch (SQLException ex)
        {
            Logger.getLogger(FormTaskList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_cboProjectActionPerformed
// test sourcetree
    private void tbTaskMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbTaskMouseClicked
        try
        {
            txtTaskReport.setText("");
            ChucNang cn = new ChucNang();
            getModel();
            int a = tbTask.getSelectedRow();
            String taskID = model.getValueAt(a, 0).toString();
            String projectName = cboProject.getSelectedItem().toString();
            String taskDescription = cn.getTaskDescription(taskID,projectName);
            String taskReport = cn.getTaskReport(taskID,projectName);
            txtTaskDescription.setText(taskDescription);
            if (!StringUtils.isEmpty(taskReport)) txtTaskReport.setText(taskReport);
            //else txtTaskReport.setText("Chưa Có Báo Cáo!!!");
        } catch (SQLException ex)
        {
            Logger.getLogger(FormTaskList.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception e){
        }
    }//GEN-LAST:event_tbTaskMouseClicked

    private void saveProject() throws SQLException{
        ChucNang cn = new ChucNang();
        getModel();
        int a = tbTask.getSelectedRow();
        String taskID = model.getValueAt(a, 0).toString();
        String projectName = cboProject.getSelectedItem().toString();
        String projectID = cn.getProjectID(projectName);
        ClassSaveDataWhenClickOnRow.getInstance().saveData(projectID,projectName, taskID); // save tên dự án và mã công việc vào form tiếp theo
    }
    private void btnTaskAssigningActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTaskAssigningActionPerformed
    try{
            getModel();
            int a = tbTask.getSelectedRow();
            String taskStatus = model.getValueAt(a, 4).toString();
            String taskReport = model.getValueAt(a, 5).toString();
            if(taskStatus.equals("Hoàn Thành")) JOptionPane.showMessageDialog(null, "Công Việc Này Đã Được Hoàn Thành"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            else if(taskStatus.equals("Đã Xóa")) JOptionPane.showMessageDialog(null, "Công Việc Này Đã Bị Xóa"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            else if(taskReport.equals("Đã Có Báo Cáo")) JOptionPane.showMessageDialog(null, "Không Thể Phân Công Công Việc Đã Có Báo Cáo"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            else {
                    saveProject();
                    new FormAssign().show();
                    dispose();
            }
        } catch (SQLException ex)
        {
            Logger.getLogger(FormTaskList.class.getName()).log(Level.SEVERE, null, ex);
        }catch (Exception e){
            if(tbTask.getSelectedRow() == -1) 
            JOptionPane.showMessageDialog(null, "Bạn chưa chọn công việc"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon2);
        }
    }//GEN-LAST:event_btnTaskAssigningActionPerformed

    private void btnTaskCompletingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTaskCompletingActionPerformed
        updateTask(FINISH_TASK);
    }//GEN-LAST:event_btnTaskCompletingActionPerformed
    
    private void btnTaskDeletingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTaskDeletingActionPerformed
        updateTask(DELETE_TASK);
    }//GEN-LAST:event_btnTaskDeletingActionPerformed

    private void btnCancelReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelReportActionPerformed
        updateTask(CANCEL_TASK);
    }//GEN-LAST:event_btnCancelReportActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(FormTaskList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(FormTaskList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex)
        {
            java.util.logging.Logger.getLogger(FormTaskList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(FormTaskList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try
                {
                    new FormTaskList().setVisible(true);
                } catch (SQLException ex)
                {
                    Logger.getLogger(FormTaskList.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelReport;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnTaskAssigning;
    private javax.swing.JButton btnTaskCompleting;
    private javax.swing.JButton btnTaskDeleting;
    private javax.swing.JComboBox<String> cboProject;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable tbTask;
    private javax.swing.JTextArea txtTaskDescription;
    private javax.swing.JTextArea txtTaskReport;
    // End of variables declaration//GEN-END:variables
}
