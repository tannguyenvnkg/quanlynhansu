/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package room;

import room.FormCreateRoom1;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import main.ChucNang;
import main.Design;
import main.Exporter;
import main.FormMain;
import personnel.FormJoiningRoomDetails1;

import project.FormProjectManager;

/**
 *
 * @author tanng
 */
public class FormRoom extends javax.swing.JFrame {
    
    //<editor-fold defaultstate="collapsed" desc=" Khởi Tạo Model combobox ">
    public DefaultComboBoxModel comboBoxModel = null;
    public DefaultComboBoxModel getComboBoxModel(){
        return comboBoxModel = (DefaultComboBoxModel)cboPhong.getModel();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Khởi Tạo Model Table ">
    public DefaultTableModel modelShowStaff_OutRoom = null;
    public DefaultTableModel getModelShowNhanVien(){
        return modelShowStaff_OutRoom = (DefaultTableModel)tableShowStaff_OutRoom.getModel();
    }
    public DefaultTableModel modelShowStaff_InRoom = null;
    public DefaultTableModel getModelLuuNhanVien(){
        return modelShowStaff_InRoom = (DefaultTableModel)tableShowStaff_InRoom.getModel();
    }
//</editor-fold>
     Icon icon1 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/error-50.png"));
     Icon icon2 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/checkmark-50.png"));
    /**
     * Creates new form FormCreateProject
     *///
    public FormRoom() throws SQLException {
        initComponents();
        tableShowStaff_OutRoom.setDefaultEditor(Object.class , null); // không được phép sửa trên jtable
        tableShowStaff_InRoom.setDefaultEditor(Object.class , null);
        tableShowStaff_OutRoom.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // chỉ được phép chọn 1 dòng trong Jtable
        tableShowStaff_InRoom.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        showRoom();
       
        //txtLeader.setEditable(false);
        // <editor-fold defaultstate="collapsed" desc="Design"> 
        this.addWindowListener(new WindowEventHandler());
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        //============== bảng màu =========================
        /*   #feffde = 254, 255, 222 = kem sữa.
              #ddffbc =  221, 255, 188 = xanh nhạt 1.
              #91c788 = 145, 199, 136 = xanh đậm 1 .
              #52734d = 82, 115, 77 = xanh đậm 2.  */
        Color tbbackground = new Color(254, 255, 222);
        Color tbforeground = new Color(0,0,0);
        Color tbselectionbackground = new Color(145, 199, 136);
        Color tbselectionforeground = new Color(255,0,0);
        //============== header Input =======================
        JTableHeader headerIn = tableShowStaff_InRoom.getTableHeader();
        headerIn.setForeground(Color.black);
        headerIn.setBackground(Color.red);
        headerIn.setFont(new Font("Times New Roman", Font.BOLD , 20));
        tableShowStaff_InRoom.setFont(new Font("Times New Roman", Font.PLAIN , 18));
        
        tableShowStaff_InRoom.setBackground(tbbackground);
        tableShowStaff_InRoom.setForeground(tbforeground);
        tableShowStaff_InRoom.setSelectionBackground(tbselectionbackground);
        tableShowStaff_InRoom.setSelectionForeground(tbselectionforeground);
        //============== header Output ======================
        JTableHeader headerOut = tableShowStaff_OutRoom.getTableHeader();
        headerOut.setForeground(Color.black);
        headerOut.setBackground(Color.red);
        headerOut.setFont(new Font("Times New Roman", Font.BOLD , 20));
        tableShowStaff_OutRoom.setFont(new Font("Times New Roman", Font.PLAIN , 18));
        
        tableShowStaff_OutRoom.setBackground(tbbackground);
        tableShowStaff_OutRoom.setForeground(tbforeground);
        tableShowStaff_OutRoom.setSelectionBackground(tbselectionbackground);
        tableShowStaff_OutRoom.setSelectionForeground(tbselectionforeground);
        
        new Design().resizeHeaderLong(tableShowStaff_InRoom);
        new Design().resizeHeaderLong(tableShowStaff_OutRoom);
           // </editor-fold>        
    }

    private void showRoom() throws SQLException{
        getComboBoxModel();
        new ChucNang().showcomboboxphong(comboBoxModel);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableShowStaff_OutRoom = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableShowStaff_InRoom = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        txttimkiem = new javax.swing.JTextField();
        btnTimKiemNhanVien = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();
        btnExport3 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        lbLeaderName = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnLuu = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        cboPhong = new javax.swing.JComboBox<>();
        btnCreateRoom = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Quản Lý Phòng");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 255, 204));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 0, 51));
        jLabel8.setText("Quản Lý Phòng");

        jPanel3.setBackground(new java.awt.Color(204, 255, 204));

        tableShowStaff_OutRoom.setBackground(new java.awt.Color(254, 255, 222));
        tableShowStaff_OutRoom.setFont(new java.awt.Font("Times New Roman", 0, 20)); // NOI18N
        tableShowStaff_OutRoom.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã Nhân Viên", "Tên Nhân Viên"
            }
        ));
        tableShowStaff_OutRoom.setFillsViewportHeight(true);
        tableShowStaff_OutRoom.setGridColor(new java.awt.Color(0, 0, 0));
        tableShowStaff_OutRoom.setSelectionBackground(new java.awt.Color(145, 199, 136));
        tableShowStaff_OutRoom.setSelectionForeground(new java.awt.Color(255, 0, 0));
        tableShowStaff_OutRoom.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tableShowStaff_OutRoom);

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));

        btnAdd.setBackground(new java.awt.Color(255, 255, 255));
        btnAdd.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-add-user-female-30.png"))); // NOI18N
        btnAdd.setText("Thêm");
        btnAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnAddMousePressed(evt);
            }
        });
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-checked-user-male-30.png"))); // NOI18N
        btnSave.setText("Lưu");
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnSaveMousePressed(evt);
            }
        });
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnReset.setBackground(new java.awt.Color(255, 255, 255));
        btnReset.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnReset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-undo-30.png"))); // NOI18N
        btnReset.setText("Làm Mới");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnDelete.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-denied-30.png"))); // NOI18N
        btnDelete.setText("Xóa");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnReset, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnDelete, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tableShowStaff_InRoom.setBackground(new java.awt.Color(254, 255, 222));
        tableShowStaff_InRoom.setFont(new java.awt.Font("Times New Roman", 0, 20)); // NOI18N
        tableShowStaff_InRoom.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã Nhân Viên", "Tên Nhân Viên"
            }
        ));
        tableShowStaff_InRoom.setFillsViewportHeight(true);
        tableShowStaff_InRoom.setGridColor(new java.awt.Color(0, 0, 0));
        tableShowStaff_InRoom.setSelectionBackground(new java.awt.Color(145, 199, 136));
        tableShowStaff_InRoom.setSelectionForeground(new java.awt.Color(255, 0, 0));
        tableShowStaff_InRoom.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tableShowStaff_InRoom);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 0, 255));
        jLabel3.setText("Danh sách nhân viên ngoài phòng :");

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 0, 255));
        jLabel1.setText("Danh sách nhân viên trong phòng :");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 506, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 506, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jPanel4.setBackground(new java.awt.Color(204, 255, 204));

        txttimkiem.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        btnTimKiemNhanVien.setBackground(new java.awt.Color(255, 255, 255));
        btnTimKiemNhanVien.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnTimKiemNhanVien.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-find-user-male-30.png"))); // NOI18N
        btnTimKiemNhanVien.setText("Tìm Kiếm");
        btnTimKiemNhanVien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTimKiemNhanVienActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txttimkiem, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnTimKiemNhanVien, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txttimkiem, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTimKiemNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(204, 255, 204));

        btnBack.setBackground(new java.awt.Color(255, 255, 255));
        btnBack.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-undo-30.png"))); // NOI18N
        btnBack.setText("Trở Lại");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnExport3.setBackground(new java.awt.Color(255, 255, 255));
        btnExport3.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnExport3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/microsoft-excel-30.png"))); // NOI18N
        btnExport3.setText("Xuất Excel");
        btnExport3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExport3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnExport3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnExport3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnBack)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBackground(new java.awt.Color(204, 255, 204));

        lbLeaderName.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setText("Leader :");

        btnLuu.setBackground(new java.awt.Color(255, 255, 255));
        btnLuu.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnLuu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-exchange-30.png"))); // NOI18N
        btnLuu.setText("Đổi Leader");
        btnLuu.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnLuu.setIconTextGap(9);
        btnLuu.setMaximumSize(new java.awt.Dimension(100, 40));
        btnLuu.setPreferredSize(new java.awt.Dimension(100, 40));
        btnLuu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLuuActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel9.setText("Chọn Phòng :");

        cboPhong.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cboPhongMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                cboPhongMouseReleased(evt);
            }
        });
        cboPhong.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboPhongActionPerformed(evt);
            }
        });

        btnCreateRoom.setBackground(new java.awt.Color(255, 255, 255));
        btnCreateRoom.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnCreateRoom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-add-user-group-man-woman-30.png"))); // NOI18N
        btnCreateRoom.setText("Tạo Phòng");
        btnCreateRoom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateRoomActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(lbLeaderName, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnLuu, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(18, 18, 18)
                        .addComponent(cboPhong, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnCreateRoom, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnLuu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbLeaderName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(cboPhong, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCreateRoom, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 13, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(204, 255, 204));

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/show-property-30.png"))); // NOI18N
        jButton1.setText("Lịch Sử Nhận Phòng");
        jButton1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(143, 143, 143)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(264, 264, 264)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 20, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cboPhongActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboPhongActionPerformed
        showStaffWithComboBoxRoom();
    }//GEN-LAST:event_cboPhongActionPerformed

    private void cboPhongMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cboPhongMouseReleased

    }//GEN-LAST:event_cboPhongMouseReleased

    private void cboPhongMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cboPhongMousePressed

    }//GEN-LAST:event_cboPhongMousePressed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        FormMain frmChinh = new FormMain();
        frmChinh.show();
        dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try
        {
            new FormJoiningRoomDetails1(this,true).show();
//            dispose();
        } catch (SQLException ex)
        {
            Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnLuuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLuuActionPerformed
        try {
            ChucNang cn = new ChucNang();
            FormChangeLeader1 frmCL = new FormChangeLeader1(this,true);
            frmCL.comboBoxModel();
            cn.showcomboboxphong(frmCL.comboBoxModel);
            frmCL.show();
//            dispose();
        } catch (SQLException ex) {
            Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnLuuActionPerformed

    private void btnCreateRoomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateRoomActionPerformed
        try {
            new FormCreateRoom().show();
            dispose();
        } catch (SQLException ex) {
            Logger.getLogger(FormRoom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnCreateRoomActionPerformed

    private void btnTimKiemNhanVienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTimKiemNhanVienActionPerformed
        try {
            String chuoi = txttimkiem.getText();
            getModelShowNhanVien();
            ChucNang cn = new ChucNang();
            String tenphong = (String) cboPhong.getSelectedItem(); // lấy tên phòng để tìm leader
            if(chuoi.toUpperCase().equals("AD1"))
            //            MessageBox.showMessageBox().showError("Đây Là Tải Khoản Dành Cho Nhà Phát Triển");
            JOptionPane.showMessageDialog(null, "Đây Là Tài Khoản Dành Cho Nhà Phát Triển","Warning",JOptionPane.INFORMATION_MESSAGE,icon1);
            else if (chuoi.equals("")) { // trống thì xuất hết
                cn.showStaffOutRoom(modelShowStaff_OutRoom,tenphong);
            }else {
                if(cn.KiemTraKieuDuLieuCuaChuoi(chuoi) == 1)  cn.SearchNhanVienTheoMa_CoMaPhong(modelShowStaff_OutRoom,chuoi,tenphong);
                else if(cn.KiemTraKieuDuLieuCuaChuoi(chuoi) == 2) cn.SearchNhanVienTheoTen_CoMaPhong(modelShowStaff_OutRoom,chuoi,tenphong);
                else if(cn.KiemTraKieuDuLieuCuaChuoi(chuoi) == 3)
                //                JOptionPane.showMessageDialog(null, "Bạn chỉ có thể nhập số hoặc chữ\n Tìm theo mã : Nhập Số \n Tìm theo tên : Nhập Tên");
                JOptionPane.showMessageDialog(null, "Bạn chỉ có thể nhập số hoặc chữ\\n Tìm theo mã : Nhập Số \\n Tìm theo tên : Nhập Tên"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
                txttimkiem.setText("");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FormRoom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnTimKiemNhanVienActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        try {
            //        DefaultTableModel tb = (DefaultTableModel)tableluunhanvien.getModel();
            //        DefaultTableModel tb2 = (DefaultTableModel)tableshownhanvien.getModel();
            getModelShowNhanVien();
            getModelLuuNhanVien();
            TableModel tb1 = tableShowStaff_InRoom.getModel();
            ChucNang cn = new ChucNang();
            int index = tableShowStaff_InRoom.getSelectedRow();
            // String tennhanvien = tb1.getValueAt(indexs[0], 1).toString();
            String tenphong = cboPhong.getSelectedItem().toString();
            Object[] options = {"Yes", "No"};
            int dialogresult = JOptionPane.showOptionDialog(null, "Xác nhân xóa?"," Warning ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon1, options, options[1]);
            //            int dialogButton = JOptionPane.YES_NO_OPTION;
            //            int dialogresult = JOptionPane.showConfirmDialog (null, "Xác nhân xóa?","Warning",dialogButton);
            if (dialogresult == 0){
                Object[] row = new Object[3];
                row[0] = tb1.getValueAt(index, 0);
                row[1] = tb1.getValueAt(index, 1);
                //row[2] = tb1.getValueAt(indexs[i], 2);
                String manhanvien = tb1.getValueAt(index, 0).toString();

                if(cn.CheckChucVu_TruyenMaNhanvien(manhanvien) != 3){
                    cn.DeleteNhanVienRaKhoiPhong_FormPhong(manhanvien,tenphong);
                    // show lại 2 table sao khi delete
                    cn.showStaffOutRoom(modelShowStaff_OutRoom, tenphong);
                    cn.showStaffInRoom(modelShowStaff_InRoom, tenphong);
                    //                    JOptionPane.showMessageDialog(null, "Xóa Nhân Viên Thành Công");
                    JOptionPane.showMessageDialog(null, "Xóa Nhân Viên Thành Công","Thông báo",JOptionPane.INFORMATION_MESSAGE,icon2);
                }else if(cn.CheckChucVu_TruyenMaNhanvien(manhanvien) == 0)
                //                    JOptionPane.showMessageDialog(null, "Lỗi");
                JOptionPane.showMessageDialog(null, "Lỗi"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
                else
                //                    JOptionPane.showMessageDialog(null, "Không Thể Xóa Leader");
                JOptionPane.showMessageDialog(null, "Không Thể Xóa Leader"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(FormRoom.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e){
            //             MessageBox.showMessageBox().showError("Vui lòng chọn nhân viên cần xóa!!!");
            JOptionPane.showMessageDialog(null, "Vui lòng chọn nhân viên cần xóa!!!"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        btnDelete.setEnabled(true);
        showStaffWithComboBoxRoom();
    }//GEN-LAST:event_btnResetActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        try {
            getModelLuuNhanVien();
            getModelShowNhanVien();
            String tenphong = cboPhong.getSelectedItem().toString();
            ChucNang cn = new ChucNang();
            ArrayList<String> chuoi = new ArrayList<String>();
            for (int i = 0; i < modelShowStaff_InRoom.getRowCount(); i++) {
                chuoi.add(modelShowStaff_InRoom.getValueAt(i, 0).toString());
            }
            cn.AddNhanVienVaoPhong(chuoi,tenphong);
            cn.showStaffOutRoom(modelShowStaff_OutRoom, tenphong);
            cn.showStaffInRoom(modelShowStaff_InRoom, tenphong);
            //            JOptionPane.showMessageDialog(null, "Lưu Thành Công");
            JOptionPane.showMessageDialog(null, "Lưu Thành Công"," Thông báo ",JOptionPane.INFORMATION_MESSAGE,icon2);
        } catch (SQLException ex) {
            Logger.getLogger(FormRoom.class.getName()).log(Level.SEVERE, null, ex);
        }
        btnDelete.setEnabled(true);
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnSaveMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSaveMousePressed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        getModelShowNhanVien();
        TableModel tb1 = tableShowStaff_OutRoom.getModel();
        DefaultTableModel tb2 = (DefaultTableModel)tableShowStaff_InRoom.getModel();

        int indexs[] = tableShowStaff_OutRoom.getSelectedRows();

        Object[] row = new Object[3];
        for (int i = 0; i < indexs.length; i++) {
            row[0] = tb1.getValueAt(indexs[i], 0);
            row[1] = tb1.getValueAt(indexs[i], 1);
            //row[2] = tb1.getValueAt(indexs[i], 2);

            tb2.addRow(row);
            modelShowStaff_OutRoom.removeRow(indexs[i]);
        }
        btnDelete.setEnabled(false);
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnAddMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddMousePressed

    }//GEN-LAST:event_btnAddMousePressed

    private class WindowEventHandler extends WindowAdapter {
                public void windowClosing(WindowEvent evt) {
         FormMain fm = new FormMain();
          fm.show();
          dispose();
  }
}    public void showStaffWithComboBoxRoom(){
        try {
            ChucNang cn = new ChucNang();
            String roomName = (String) cboPhong.getSelectedItem(); // lấy tên phòng để tìm leader
            lbLeaderName.setText(cn.TenLeader(roomName)); // set tên leader cho text
            
            getModelShowNhanVien();
            getModelLuuNhanVien();
            cn.showStaffOutRoom(modelShowStaff_OutRoom,roomName);
            cn.showStaffInRoom(modelShowStaff_InRoom,roomName);
        } catch (SQLException ex) {
            Logger.getLogger(FormRoom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void btnExport3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExport3ActionPerformed
        getModelLuuNhanVien();
        new Exporter().exportExcel(modelShowStaff_InRoom);
    }//GEN-LAST:event_btnExport3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormRoom.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormRoom.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormRoom.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormRoom.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try
                {
                    new FormRoom().setVisible(true);
                } catch (SQLException ex)
                {
                    Logger.getLogger(FormRoom.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCreateRoom;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnExport3;
    private javax.swing.JButton btnLuu;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnTimKiemNhanVien;
    private javax.swing.JComboBox<String> cboPhong;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lbLeaderName;
    private javax.swing.JTable tableShowStaff_InRoom;
    private javax.swing.JTable tableShowStaff_OutRoom;
    private javax.swing.JTextField txttimkiem;
    // End of variables declaration//GEN-END:variables
}
