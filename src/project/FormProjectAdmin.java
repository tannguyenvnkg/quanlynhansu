/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import main.ChucNang;
import main.Design;
import main.FormMain;
import main.NhanVien;

/**
 *
 * @author Admin
 */
public class FormProjectAdmin extends javax.swing.JFrame {
    Icon icon1 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/error-50.png"));
    Icon icon2 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/checkmark-50.png"));
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    //<editor-fold defaultstate="collapsed" desc=" Khởi Tạo Model Table ">
    DefaultTableModel model = null;
    public final DefaultTableModel getModel(){
        return model = (DefaultTableModel)tbProjectForAdmin.getModel();
    }
    DefaultTableModel modelStaffInProject = null;
    public final DefaultTableModel getModelStaffInProject(){
        return modelStaffInProject = (DefaultTableModel)tbStaffInProject.getModel();
    }
    //</editor-fold>
    /**
     * Creates new form FormDuAn
     */
    public FormProjectAdmin() throws SQLException {
        initComponents();
        tbProjectForAdmin.setDefaultEditor(Object.class , null); // không edit được dòng trong jtable
        tbProjectForAdmin.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // chỉ được phép chọn 1 dòng trong Jtable
        tbStaffInProject.setDefaultEditor(Object.class , null); // không edit được dòng trong jtable
        tbStaffInProject.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // chỉ được phép chọn 1 dòng trong Jtable
        ChucNang cn = new ChucNang();
        getModel();
        cn.showProject(model);        
        if(NhanVien.getInstance().getMachucvu() == 1) { //ẩn  2 button lưu và hoàn thành trong form
            btnProjectCompletion.setVisible(false);
            btnSave.setVisible(false);
        }
        // <editor-fold defaultstate="collapsed" desc="Design"> 
        this.addWindowListener(new WindowEventHandler());
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        //============== bảng màu =========================
        /*   #feffde = 254, 255, 222 = kem sữa.
              #ddffbc =  221, 255, 188 = xanh nhạt 1.
              #91c788 = 145, 199, 136 = xanh đậm 1 .
              #52734d = 82, 115, 77 = xanh đậm 2.  */
        Color tbbackground = new Color(254, 255, 222);
        Color tbforeground = new Color(0,0,0);
        Color tbselectionbackground = new Color(145, 199, 136);
        Color tbselectionforeground = new Color(255,0,0);
        //============== header Project =======================
        JTableHeader headerIn = tbProjectForAdmin.getTableHeader();
        headerIn.setForeground(Color.black);
        headerIn.setBackground(Color.red);
        headerIn.setFont(new Font("Times New Roman", Font.BOLD , 20));
        tbProjectForAdmin.setFont(new Font("Times New Roman", Font.PLAIN , 18));
        
        tbProjectForAdmin.setBackground(tbbackground);
        tbProjectForAdmin.setForeground(tbforeground);
        tbProjectForAdmin.setSelectionBackground(tbselectionbackground);
        tbProjectForAdmin.setSelectionForeground(tbselectionforeground);
        
        //============== header Staff =======================
        JTableHeader headerOut = tbStaffInProject.getTableHeader();
        headerOut.setForeground(Color.black);
        headerOut.setBackground(Color.red);
        headerOut.setFont(new Font("Times New Roman", Font.BOLD , 20));
        tbStaffInProject.setFont(new Font("Times New Roman", Font.PLAIN , 18));
        
        tbStaffInProject.setBackground(tbbackground);
        tbStaffInProject.setForeground(tbforeground);
        tbStaffInProject.setSelectionBackground(tbselectionbackground);
        tbStaffInProject.setSelectionForeground(tbselectionforeground);
        //============== set JTable =======================
        new Design().resizeHeaderLong(tbProjectForAdmin);
        new Design().resizeHeaderLong(tbStaffInProject);
        tbProjectForAdmin.getColumnModel().getColumn(0).setCellRenderer(new Design.AlignRenderer());
        tbProjectForAdmin.getColumnModel().getColumn(3).setCellRenderer(new Design.AlignRenderer());
        tbProjectForAdmin.getColumnModel().getColumn(4).setCellRenderer(new Design.AlignRenderer());
        tbProjectForAdmin.getColumnModel().getColumn(5).setCellRenderer(new Design.AlignRenderer());
        tbStaffInProject.getColumnModel().getColumn(0).setCellRenderer(new Design.AlignRenderer());
        tbStaffInProject.getColumnModel().getColumn(2).setCellRenderer(new Design.AlignRenderer());
        tbProjectForAdmin.getColumnModel().getColumn(0).setPreferredWidth(150);
        tbProjectForAdmin.getColumnModel().getColumn(1).setPreferredWidth(230);
        tbProjectForAdmin.getColumnModel().getColumn(2).setPreferredWidth(210);
        tbProjectForAdmin.getColumnModel().getColumn(3).setPreferredWidth(200);
        tbProjectForAdmin.getColumnModel().getColumn(4).setPreferredWidth(200);
        tbProjectForAdmin.getColumnModel().getColumn(5).setPreferredWidth(200);
        tbProjectForAdmin.getColumnModel().getColumn(6).setPreferredWidth(200);
        // </editor-fold>
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtProjectID = new javax.swing.JTextField();
        txtProjectName = new javax.swing.JTextField();
        txtProjectStatus = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbStaffInProject = new javax.swing.JTable();
        txtStartingProjectDate = new com.toedter.calendar.JDateChooser();
        txtEndingProjectDate = new com.toedter.calendar.JDateChooser();
        txtProjectManagerName = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtProjectDescription = new javax.swing.JTextArea();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbProjectForAdmin = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        btnSearch = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        btnProjectCompletion = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Thông Tin Dự Án");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 255, 204));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("Thông Tin Dự Án");

        jSeparator1.setForeground(new java.awt.Color(51, 51, 51));
        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel4.setText("Mã Dự Án :");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel5.setText("Tên Dự Án :");

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel6.setText("Thời Gian Bắt Đầu :");

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel7.setText("Thời Gian Kết Thúc :");

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel8.setText("Trạng Thái :");

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel9.setText("Danh Sách Nhân Viên :");

        txtProjectID.setEditable(false);
        txtProjectID.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        txtProjectName.setEditable(false);
        txtProjectName.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        txtProjectStatus.setEditable(false);
        txtProjectStatus.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        tbStaffInProject.setBackground(new java.awt.Color(254, 255, 222));
        tbStaffInProject.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        tbStaffInProject.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã Nhân Viên", "Họ Tên", "Chức Vụ"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbStaffInProject.setFillsViewportHeight(true);
        tbStaffInProject.setGridColor(new java.awt.Color(0, 0, 0));
        tbStaffInProject.setSelectionBackground(new java.awt.Color(145, 199, 136));
        tbStaffInProject.setSelectionForeground(new java.awt.Color(255, 0, 0));
        tbStaffInProject.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tbStaffInProject);

        txtProjectManagerName.setEditable(false);
        txtProjectManagerName.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        txtProjectDescription.setEditable(false);
        txtProjectDescription.setColumns(20);
        txtProjectDescription.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        txtProjectDescription.setLineWrap(true);
        txtProjectDescription.setRows(5);
        txtProjectDescription.setWrapStyleWord(true);
        jScrollPane3.setViewportView(txtProjectDescription);

        jLabel10.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel10.setText("Tên Project Maneger :");

        jLabel11.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel11.setText("Mô tả :");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 255));
        jLabel3.setText("Thông Tin Dự Án");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel9))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel6)))
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtProjectStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtProjectID, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(39, 39, 39)
                                        .addComponent(jLabel11))
                                    .addComponent(txtProjectName, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtProjectManagerName, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtStartingProjectDate, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtEndingProjectDate, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane2))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtProjectID, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtProjectName, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtProjectManagerName, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(txtStartingProjectDate, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(jLabel6)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(txtEndingProjectDate, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(jLabel7))))
                    .addComponent(jScrollPane3))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtProjectStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addComponent(jLabel9)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(204, 255, 204));

        tbProjectForAdmin.setBackground(new java.awt.Color(254, 255, 222));
        tbProjectForAdmin.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        tbProjectForAdmin.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Mã Dự Án", "Tên Dự Án", "Project Manager", "Thời Gian Bắt Đầu", "Thời Gian Kết Thúc", "Trạng Thái", "Phòng"
            }
        ));
        tbProjectForAdmin.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_NEXT_COLUMN);
        tbProjectForAdmin.setFillsViewportHeight(true);
        tbProjectForAdmin.setGridColor(new java.awt.Color(0, 0, 0));
        tbProjectForAdmin.setSelectionBackground(new java.awt.Color(145, 199, 136));
        tbProjectForAdmin.setSelectionForeground(new java.awt.Color(255, 0, 0));
        tbProjectForAdmin.getTableHeader().setReorderingAllowed(false);
        tbProjectForAdmin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbProjectForAdminMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbProjectForAdmin);
        if (tbProjectForAdmin.getColumnModel().getColumnCount() > 0) {
            tbProjectForAdmin.getColumnModel().getColumn(3).setResizable(false);
        }

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 255));
        jLabel2.setText("Danh Sách Dự Án");

        jPanel7.setBackground(new java.awt.Color(204, 255, 204));

        btnSearch.setBackground(new java.awt.Color(255, 255, 255));
        btnSearch.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Favorites/Edit/icons8-search-property-30.png"))); // NOI18N
        btnSearch.setText("Tìm Kiếm");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSearch)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnSearch))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 852, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2))
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(204, 255, 204));

        btnProjectCompletion.setBackground(new java.awt.Color(255, 255, 255));
        btnProjectCompletion.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnProjectCompletion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/task-completed-30.png"))); // NOI18N
        btnProjectCompletion.setText("Hoàn Thành");
        btnProjectCompletion.setMaximumSize(new java.awt.Dimension(145, 40));
        btnProjectCompletion.setMinimumSize(new java.awt.Dimension(145, 40));
        btnProjectCompletion.setPreferredSize(new java.awt.Dimension(145, 40));
        btnProjectCompletion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProjectCompletionActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/save-30.png"))); // NOI18N
        btnSave.setText("Lưu");
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnSave.setIconTextGap(20);
        btnSave.setMaximumSize(new java.awt.Dimension(145, 40));
        btnSave.setMinimumSize(new java.awt.Dimension(145, 40));
        btnSave.setPreferredSize(new java.awt.Dimension(145, 40));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnProjectCompletion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnProjectCompletion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBackground(new java.awt.Color(204, 255, 204));

        btnBack.setBackground(new java.awt.Color(255, 255, 255));
        btnBack.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/undo-30.png"))); // NOI18N
        btnBack.setText("Trở Lại");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(288, 288, 288)
                                .addComponent(jLabel1)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 13, Short.MAX_VALUE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(66, 66, 66)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator1)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private class WindowEventHandler extends WindowAdapter {
                public void windowClosing(WindowEvent evt) {
         FormMain fm = new FormMain();
          fm.show();
          dispose();
  }
}
    private void clear(){
        txtProjectID.setText("");
        txtProjectName.setText("");
        txtProjectStatus.setText("");
        txtProjectDescription.setText("");
        txtProjectManagerName.setText("");
        txtStartingProjectDate.setDate(null);
        txtEndingProjectDate.setDate(null);
    }
    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        FormMain frmChinh = new FormMain();
        frmChinh.show();
        dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void tbProjectForAdminMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbProjectForAdminMouseClicked
        try{
            ChucNang cn = new ChucNang();
            getModel(); 
            getModelStaffInProject();
            int a = tbProjectForAdmin.getSelectedRow();
            txtProjectID.setText((String) model.getValueAt(a, 0));
            txtProjectName.setText((String) model.getValueAt(a, 1));
            txtProjectManagerName.setText((String) model.getValueAt(a, 2));
            txtStartingProjectDate.setDate(date.parse(model.getValueAt(a, 3).toString()));
            //if(!model.getValueAt(a, 3).toString().equals("")) 
            txtEndingProjectDate.setDate(date.parse(model.getValueAt(a, 4).toString()));
            txtProjectStatus.setText((String) model.getValueAt(a, 5));
            txtProjectDescription.setText(cn.getProjectDescription((String) model.getValueAt(a, 0))); // lấy mã dự án để show mô tả dự án tại dự án được chọn
            //==================================================================================================
            String roomName = (String) model.getValueAt(a, 6);
            cn.showStaffInProject(modelStaffInProject,roomName);
        } catch (ParseException ex){
            Logger.getLogger(FormProjectManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex)
        {
            Logger.getLogger(FormProjectManager.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_tbProjectForAdminMouseClicked

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        try{
            String chuoi = txtSearch.getText();
            getModel();
            ChucNang cn = new ChucNang();
            if (chuoi.equals("")) { // trống thì xuất hết
                cn.showProject(model);
            }
            if(cn.KiemTraKieuDuLieuCuaChuoi(chuoi) == 1)  cn.searchProjectWithProjectID(model,chuoi);
            else if(cn.KiemTraKieuDuLieuCuaChuoi(chuoi) == 2) cn.searchProjectWithProjectName(model,chuoi);
            else if(cn.KiemTraKieuDuLieuCuaChuoi(chuoi) == 3) 
                JOptionPane.showMessageDialog(null, "Bạn chỉ có thể nhập số hoặc chữ\n Tìm theo mã : Nhập Số \n Tìm theo tên : Nhập Tên"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
            txtSearch.setText("");
        } catch (Exception e){
        }
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnProjectCompletionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProjectCompletionActionPerformed
        try
        {
            ChucNang cn = new ChucNang();
            getModel();
            int a = tbProjectForAdmin.getSelectedRow();
            String projectID = model.getValueAt(a, 0).toString();
            cn.finishProject(projectID,model);
            cn.showProject(model);
            clear();
            //MessageBox.showMessageBox().showSuccess("Cập Nhật Dự Án Thành Công");
//            JOptionPane.showMessageDialog(null, "Cập Nhật Dự Án Thành Công","Thông báo",JOptionPane.INFORMATION_MESSAGE,icon2);
        } catch (Exception e)
        {
            if(tbProjectForAdmin.getSelectedRow() == -1) 
//                MessageBox.showMessageBox().showError("Bạn Chưa Chọn Dự Án");
                JOptionPane.showMessageDialog(null, "Bạn Chưa Chọn Dự Án"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
            clear();
        }
    }//GEN-LAST:event_btnProjectCompletionActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        try{
            ChucNang cn = new ChucNang();
            getModel();
            String startingProjectDate = "";
            String endingProjectDate = "";
            int a = tbProjectForAdmin.getSelectedRow();
            String projectID = (String) model.getValueAt(a, 0);
            if(txtStartingProjectDate.getDate() != null) startingProjectDate = date.format(txtStartingProjectDate.getDate());
            if(txtEndingProjectDate.getDate() != null) endingProjectDate = date.format(txtEndingProjectDate.getDate());
            
            if(txtStartingProjectDate.getDate().after(txtEndingProjectDate.getDate())) 
//                MessageBox.showMessageBox().showError("Ngày Kết Thúc Phải Nhỏ Hơn Ngày Bắt Đầu");
                JOptionPane.showMessageDialog(null, "Ngày Kết Thúc Phải Nhỏ Hơn Ngày Bắt Đầu"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
            else {
                cn.saveProject(projectID,startingProjectDate,endingProjectDate);
                cn.showProject(model); // load lại dự án
                clear();
            }
        } catch (Exception e){
            if(tbProjectForAdmin.getSelectedRow() == -1 || txtProjectID.equals("")) 
//                MessageBox.showMessageBox().showError("Bạn Chưa Chọn Dự Án");
            JOptionPane.showMessageDialog(null, "Bạn Chưa Chọn Dự Án"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
            else 
//                MessageBox.showMessageBox().showError("Ngày Điền Vào Không Hợp Lệ");
                JOptionPane.showMessageDialog(null, "Ngày Điền Vào Không Hợp Lệ"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
            clear();
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormProjectAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormProjectAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormProjectAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormProjectAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try
                {
                    new FormProjectAdmin().setVisible(true);
                } catch (SQLException ex)
                {
                    Logger.getLogger(FormProjectAdmin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnProjectCompletion;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable tbProjectForAdmin;
    private javax.swing.JTable tbStaffInProject;
    private com.toedter.calendar.JDateChooser txtEndingProjectDate;
    private javax.swing.JTextArea txtProjectDescription;
    private javax.swing.JTextField txtProjectID;
    private javax.swing.JTextField txtProjectManagerName;
    private javax.swing.JTextField txtProjectName;
    private javax.swing.JTextField txtProjectStatus;
    private javax.swing.JTextField txtSearch;
    private com.toedter.calendar.JDateChooser txtStartingProjectDate;
    // End of variables declaration//GEN-END:variables
}
