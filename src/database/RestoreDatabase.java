/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author si
 */
public class RestoreDatabase {
    Connection conn = null; // khỏi tạo global conn
    public Statement stmt = null; // khỏi tạo global stmt
    public void connect(){

        try {
            String ServerDBName = "localhost";
            String databaseName = "master";
            String user = "sa";
            String password = "123456";
            String dbURL = "jdbc:sqlserver://"+ ServerDBName +";databaseName="+ databaseName + ";user=" + user + ";password="+password; 

            conn = DriverManager.getConnection(dbURL); // nối database
            stmt = conn.createStatement(); // tạo stmt
            if(conn != null) System.out.println("Connect Database Successful");
       } catch (SQLException e) {
            System.err.println(e);
        }
    }
}