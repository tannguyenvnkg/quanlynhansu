/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personnel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import login.profile.FormLogin;
import main.ChucNang;
import main.Design;
import main.Exporter;
import main.FormMain;

/**
 *
 * @author Admin
 */
public class NhanSu extends javax.swing.JFrame {

    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    //<editor-fold defaultstate="collapsed" desc=" Khởi Tạo Model Table ">
    public DefaultTableModel model = null;
    public DefaultTableModel getModel(){
        return model = (DefaultTableModel)tablenhanvien.getModel();
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Khởi Tạo Model combobox ">
    public DefaultComboBoxModel comboBoxModel = null;
    public DefaultComboBoxModel comboBoxModel(){
        return comboBoxModel = (DefaultComboBoxModel)cboChucVu.getModel();
    }
    //</editor-fold>
      Icon icon1 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/checkmark-50.png"));
      Icon icon2 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/error-50.png"));
    /**
     * Creates new form NhanSu
     */
    public NhanSu() throws SQLException {
        initComponents();
        tablenhanvien.setDefaultEditor(Object.class , null); // không edit được dòng trong jtable
        tablenhanvien.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // chỉ được phép chọn 1 dòng trong Jtable
        showStaff();
        design();
        //tiền thưởng chỉ nhập được số
        typeOnlyNumber();
        
    }
 private void design(){
      // <editor-fold defaultstate="collapsed" desc="Design"> 
        //=============== Closebutton =======================
        this.addWindowListener(new WindowEventHandler());
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
         //============== bảng màu =========================
        /*   #feffde = 254, 255, 222 = kem sữa.
              #ddffbc =  221, 255, 188 = xanh nhạt 1.
              #91c788 = 145, 199, 136 = xanh đậm 1 .
              #52734d = 82, 115, 77 = xanh đậm 2.  */
        Color tbbackground = new Color(254, 255, 222);
        Color tbforeground = new Color(0,0,0);
        Color tbselectionbackground = new Color(145, 199, 136);
        Color tbselectionforeground = new Color(255,0,0);
         //============== header Input =======================
        JTableHeader header = tablenhanvien.getTableHeader();
        header.setForeground(Color.black);
        header.setBackground(Color.RED);
        header.setFont(new Font("Times New Roman", Font.BOLD , 20));
        tablenhanvien.setFont(new Font("Times New Roman", Font.PLAIN , 18));
        
        tablenhanvien.setBackground(tbbackground);
        tablenhanvien.setForeground(tbforeground);
        tablenhanvien.setSelectionBackground(tbselectionbackground);
        tablenhanvien.setSelectionForeground(tbselectionforeground);
        //============== st JTable =======================
        new Design().resizeHeaderLong(tablenhanvien);
        tablenhanvien.getColumnModel().getColumn(4).setCellRenderer(new Design.ImageRenderer());
        tablenhanvien.getColumnModel().getColumn(0).setCellRenderer(new Design.AlignRenderer());
        tablenhanvien.getColumnModel().getColumn(2).setCellRenderer(new Design.AlignRenderer());
        tablenhanvien.getColumnModel().getColumn(3).setCellRenderer(new Design.AlignRenderer());
        tablenhanvien.getColumnModel().getColumn(0).setPreferredWidth(170);
        tablenhanvien.getColumnModel().getColumn(1).setPreferredWidth(230);
        tablenhanvien.getColumnModel().getColumn(2).setPreferredWidth(180);
        tablenhanvien.getColumnModel().getColumn(3).setPreferredWidth(180);
        tablenhanvien.getColumnModel().getColumn(4).setPreferredWidth(177);
        // </editor-fold>
 }
    private void showStaff() throws SQLException{
        ChucNang cn = new ChucNang();
        getModel(); //lấy jtable
        comboBoxModel(); // lấy combobox
        cn.showNhanVien(model);  // show nhân viên vào jtable
        cn.showcomboboxchucvu(comboBoxModel);
        disable();
    }
     private class WindowEventHandler extends WindowAdapter {
                public void windowClosing(WindowEvent evt) {
         FormMain fm = new FormMain();
          fm.show();
          dispose();
  }
}
    public final void typeOnlyNumber(){
        txtBonus.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char caracter = e.getKeyChar();
                if (((caracter < '0') || (caracter > '9'))
                        && (caracter != '\b')) {
                    e.consume();
                }
            }
        });
        txtsdt.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char caracter = e.getKeyChar();
                if (((caracter < '0') || (caracter > '9'))
                        && (caracter != '\b')) {
                    e.consume();
                }
            }
        });
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rbtngioitinh = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtmanhanvien = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txttennhanvien = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtngaysinh = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        txtdiachi = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtsdt = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        cboChucVu = new javax.swing.JComboBox<>();
        rbtnNam = new javax.swing.JRadioButton();
        rbtnNu = new javax.swing.JRadioButton();
        jLabel7 = new javax.swing.JLabel();
        txtBonus = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        btnresetpassword = new javax.swing.JButton();
        btnAddNew = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnActive = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        btngetback = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablenhanvien = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        txttimkiem = new javax.swing.JTextField();
        btnTimKiem = new javax.swing.JButton();
        btnUpdateHistory = new javax.swing.JButton();
        btnExport = new javax.swing.JButton();
        btnStaffDetail = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Quản Lý Nhân Sự");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 255, 204));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 0, 51));
        jLabel8.setText("Quản Lý Nhân Sự");

        jPanel3.setBackground(new java.awt.Color(204, 255, 204));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel1.setText("Mã Nhân Viên :");

        txtmanhanvien.setEditable(false);
        txtmanhanvien.setBackground(new java.awt.Color(255, 255, 255));
        txtmanhanvien.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel2.setText("Giới Tính :");

        txttennhanvien.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        txttennhanvien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttennhanvienKeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel4.setText("Ngày Sinh :");

        txtngaysinh.setPreferredSize(new java.awt.Dimension(220, 22));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel5.setText("Địa Chỉ :");

        txtdiachi.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel3.setText("SĐT : ");
        jLabel3.setToolTipText("");

        txtsdt.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel6.setText("Chức Vụ :");

        cboChucVu.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cboChucVu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboChucVuActionPerformed(evt);
            }
        });

        rbtnNam.setBackground(new java.awt.Color(204, 255, 204));
        rbtngioitinh.add(rbtnNam);
        rbtnNam.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        rbtnNam.setText("Nam");

        rbtnNu.setBackground(new java.awt.Color(204, 255, 204));
        rbtngioitinh.add(rbtnNu);
        rbtnNu.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        rbtnNu.setText("Nữ");

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel7.setText("Họ Tên :");

        txtBonus.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        jLabel11.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel11.setText("tiền thưởng :");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(rbtnNam)
                        .addGap(59, 59, 59)
                        .addComponent(rbtnNu))
                    .addComponent(txtmanhanvien)
                    .addComponent(txttennhanvien)
                    .addComponent(txtngaysinh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtdiachi)
                    .addComponent(txtsdt)
                    .addComponent(cboChucVu, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtBonus)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtmanhanvien, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txttennhanvien, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(rbtnNam, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rbtnNu, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtngaysinh, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel4)))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtdiachi, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtsdt, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboChucVu, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBonus, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(204, 255, 204));

        btnresetpassword.setBackground(new java.awt.Color(255, 255, 255));
        btnresetpassword.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnresetpassword.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-undo-30.png"))); // NOI18N
        btnresetpassword.setText("Đặt Lại Mật Khẩu");
        btnresetpassword.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnresetpassword.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnresetpassword.setMinimumSize(new java.awt.Dimension(91, 25));
        btnresetpassword.setPreferredSize(new java.awt.Dimension(109, 40));
        btnresetpassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnresetpasswordActionPerformed(evt);
            }
        });

        btnAddNew.setBackground(new java.awt.Color(255, 255, 255));
        btnAddNew.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnAddNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-add-user-female-30.png"))); // NOI18N
        btnAddNew.setText("Làm Mới");
        btnAddNew.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnAddNew.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAddNew.setIconTextGap(25);
        btnAddNew.setPreferredSize(new java.awt.Dimension(91, 40));
        btnAddNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-checked-user-male-30.png"))); // NOI18N
        btnSave.setText(" Lưu");
        btnSave.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnSave.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnSave.setIconTextGap(30);
        btnSave.setPreferredSize(new java.awt.Dimension(91, 40));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnDelete.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-denied-30.png"))); // NOI18N
        btnDelete.setText("Khóa Tài Khoản");
        btnDelete.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnDelete.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnDelete.setIconTextGap(10);
        btnDelete.setPreferredSize(new java.awt.Dimension(91, 40));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnActive.setBackground(new java.awt.Color(255, 255, 255));
        btnActive.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnActive.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-key-2-30.png"))); // NOI18N
        btnActive.setToolTipText("");
        btnActive.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnActive.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnActive.setIconTextGap(23);
        btnActive.setLabel("Kích Hoạt");
        btnActive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActiveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnDelete, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAddNew, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnresetpassword, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                    .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnActive, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnresetpassword, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(btnAddNew, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addComponent(btnActive, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(204, 255, 204));

        btngetback.setBackground(new java.awt.Color(255, 255, 255));
        btngetback.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-undo-30.png"))); // NOI18N
        btngetback.setText("Trở Lại");
        btngetback.setMinimumSize(new java.awt.Dimension(91, 25));
        btngetback.setPreferredSize(new java.awt.Dimension(109, 40));
        btngetback.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btngetbackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btngetback, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btngetback, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 51, 255));
        jLabel9.setText("Thông Tin Nhân Viên");
        jLabel9.setToolTipText("");

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(51, 51, 255));
        jLabel10.setText("Danh Sách Nhân Viên");
        jLabel10.setToolTipText("");

        jSeparator1.setForeground(new java.awt.Color(51, 51, 51));
        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));

        tablenhanvien.setBackground(new java.awt.Color(254, 255, 222));
        tablenhanvien.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        tablenhanvien.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Mã ", "Họ Tên", "Giới Tính", "Chức Vụ", "Trạng Thái"
            }
        ));
        tablenhanvien.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_LAST_COLUMN);
        tablenhanvien.setFillsViewportHeight(true);
        tablenhanvien.setGridColor(new java.awt.Color(0, 0, 0));
        tablenhanvien.setSelectionBackground(new java.awt.Color(145, 199, 136));
        tablenhanvien.setSelectionForeground(new java.awt.Color(255, 0, 0));
        tablenhanvien.getTableHeader().setReorderingAllowed(false);
        tablenhanvien.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablenhanvienMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablenhanvien);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 958, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 456, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(204, 255, 204));

        txttimkiem.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        txttimkiem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txttimkiemKeyPressed(evt);
            }
        });

        btnTimKiem.setBackground(new java.awt.Color(255, 255, 255));
        btnTimKiem.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnTimKiem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-find-user-male-30.png"))); // NOI18N
        btnTimKiem.setText("Tìm Kiếm");
        btnTimKiem.setPreferredSize(new java.awt.Dimension(91, 40));
        btnTimKiem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTimKiemActionPerformed(evt);
            }
        });

        btnUpdateHistory.setBackground(new java.awt.Color(255, 255, 255));
        btnUpdateHistory.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/show-property-30.png"))); // NOI18N
        btnUpdateHistory.setText("Lịch Sử Cập Nhật");
        btnUpdateHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateHistoryActionPerformed(evt);
            }
        });

        btnExport.setBackground(new java.awt.Color(255, 255, 255));
        btnExport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/microsoft-excel-30.png"))); // NOI18N
        btnExport.setText("Xuất Excel");
        btnExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportActionPerformed(evt);
            }
        });

        btnStaffDetail.setBackground(new java.awt.Color(255, 255, 255));
        btnStaffDetail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/show-property-30.png"))); // NOI18N
        btnStaffDetail.setText("Xem Chi Tiết");
        btnStaffDetail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStaffDetailActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txttimkiem, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnUpdateHistory, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnExport)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnStaffDetail)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txttimkiem, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnTimKiem, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnUpdateHistory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnExport, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnStaffDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        btnStaffDetail.getAccessibleContext().setAccessibleDescription("");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(257, 257, 257))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(686, 686, 686)
                .addComponent(jLabel8)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 542, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10))
                                .addGap(33, 33, 33))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void setStaffToTextBox() throws SQLException, ParseException{
        getModel();
        ChucNang cn = new ChucNang();
        int a = tablenhanvien.getSelectedRow();
        String staffID = model.getValueAt(a, 0).toString();
        ArrayList<String> staff = cn.getStaff(staffID); 
       
        txtmanhanvien.setText(staff.get(0));
        txttennhanvien.setText(staff.get(1));
        txtngaysinh.setDate(date.parse(staff.get(2)));  //txt ngày sinh
        txtdiachi.setText(staff.get(3));  // txt địa chỉ
        txtsdt.setText(staff.get(4));  // txt sdt
        if(Integer.parseInt(staff.get(5)) == 1) rbtnNam.setSelected(true); // radiobutton giới tính
        else rbtnNu.setSelected(true);
        cboChucVu.setSelectedIndex(Integer.parseInt(staff.get(7)) - 1); // combobox chức vụ
        txtBonus.setText(staff.get(8)); //txt tiền thưởng
    }
    private void tablenhanvienMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablenhanvienMouseClicked
        try {
//            enable();
//            getModel();
//            //txtmanhanvien.disable();
//            txtngaysinh.enable();;
//            int a = tablenhanvien.getSelectedRow();
//            txtmanhanvien.setText(model.getValueAt(a, 0).toString());
//            txttennhanvien.setText(model.getValueAt(a, 1).toString());
//            if((model.getValueAt(a, 2).toString()).equals("Nam")) rbtnNam.setSelected(true); else rbtnNu.setSelected(true);
//
//            txtngaysinh.setDate(date.parse((model.getValueAt(a, 3)).toString()));
//            txtsdt.setText(model.getValueAt(a, 4).toString());
//            txtdiachi.setText(model.getValueAt(a, 5).toString());
//            //  txtmatkhau.setText(getModel.getValueAt(a, 6).toString());
//            cboChucVu.setSelectedItem(model.getValueAt(a, 6).toString());
//            txtBonus.setText(model.getValueAt(a, 7).toString());
            setStaffToTextBox();
        } catch (ParseException ex) {
            Logger.getLogger(NhanSu.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex)
        {
            Logger.getLogger(NhanSu.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_tablenhanvienMouseClicked

    private void btnTimKiemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTimKiemActionPerformed
        String chuoi = txttimkiem.getText();
        getModel();
        ChucNang cn = new ChucNang();
        if(chuoi.toUpperCase().equals("AD1"))
             JOptionPane.showMessageDialog(null, "Đây Là Tải Khoản Dành Cho Nhà Phát Triển"," Warning ",JOptionPane.INFORMATION_MESSAGE,icon2);
        else if (chuoi.equals("")) { // trống thì xuất hết
            cn.shownhanvien_Admin(model);
        }else {
            if(cn.KiemTraKieuDuLieuCuaChuoi(chuoi) == 1)  cn.SearchNhanVienTheoMa(model,chuoi);
            else if(cn.KiemTraKieuDuLieuCuaChuoi(chuoi) == 2) cn.SearchNhanVienTheoTen(model,chuoi);
            else if(cn.KiemTraKieuDuLieuCuaChuoi(chuoi) == 3) 
                JOptionPane.showMessageDialog(null, "Bạn chỉ có thể nhập số hoặc chữ\\n Tìm theo mã : Nhập Số \\n Tìm theo tên : Nhập Tên"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            txttimkiem.setText("");
        }
    }//GEN-LAST:event_btnTimKiemActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        try {
            ChucNang cn = new ChucNang();
            String ma = txtmanhanvien.getText();
            if(!ma.equals("")){
                Object[] options = {"Yes", "No"}; // hỏi yes no để xác nhận xóa
                int dialogresult = JOptionPane.showOptionDialog(null, "Xác nhân khóa tài khoản?"," Warning ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon1, options, options[1]);
                if (dialogresult == 0) cn.disableStaff(ma);
            }else JOptionPane.showMessageDialog(null, "Vui Lòng Chọn Nhân Viên Để Khóa"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            clear();
            cn.showNhanVien(model);
        } catch (SQLException ex) {
            Logger.getLogger(NhanSu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        try {
            getModel();
            String ngaysinh = "";
            boolean gioitinh = true;
            ChucNang cn = new ChucNang();
            String ma = txtmanhanvien.getText();
            String ten = txttennhanvien.getText();
            if(rbtnNu.isSelected()) gioitinh = false;
            ngaysinh = date.format(txtngaysinh.getDate());
            String sdt = txtsdt.getText();
            String diachi = txtdiachi.getText();
            //String matkhau = txtmatkhau.getText();
            String chucvu = (String) cboChucVu.getSelectedItem();
            String bonus = txtBonus.getText();
            int machucvu = cn.getPositionID(chucvu);
            if(machucvu != 4  && cn.stayInRoom(ma)){
                JOptionPane.showMessageDialog(null, "Nhân Viên Phải Rời Phòng Trước Khi Thay Đổi Chức Vụ"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            }else if(bonus.equals("")){
                JOptionPane.showMessageDialog(null, "Vui lòng điền thưởng cho 1 công việc"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            }else if(!ten.equals("")){
                if(!cn.checkma(ma)) cn.addStaff(/*ma,*/ten,ngaysinh,diachi,sdt,gioitinh,/*,matkhau*/chucvu,bonus,model);
                else cn.updateStaff(ma,ten,ngaysinh,diachi,sdt,gioitinh,chucvu,bonus,model);
                clear();
                //cn.showNhanVien(model);
            }else 
                  JOptionPane.showMessageDialog(null, "Tên Nhân Viên Không Được Để trống!!!"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);

        } catch (SQLException ex) {
            Logger.getLogger(NhanSu.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(NhanSu.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Ngày Sinh Điền Vào Không Hợp Lệ"," Error",JOptionPane.INFORMATION_MESSAGE,icon2);
        }
    }//GEN-LAST:event_btnSaveActionPerformed
    
    private void btnAddNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewActionPerformed
        enable();
        clear();
        //txtmanhanvien.disable();
    }//GEN-LAST:event_btnAddNewActionPerformed

    private void btnresetpasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnresetpasswordActionPerformed
        try {
            String ma = txtmanhanvien.getText();
            if(!ma.equals("")){
                ChucNang cn = new ChucNang();
                cn.resetpassword(ma);
            }
            else
                JOptionPane.showMessageDialog(null, "Vui Lòng Chọn Nhân Viên"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(NhanSu.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(NhanSu.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_btnresetpasswordActionPerformed

    private void cboChucVuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboChucVuActionPerformed

    }//GEN-LAST:event_cboChucVuActionPerformed

    private void btngetbackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btngetbackActionPerformed
        //<editor-fold defaultstate="collapsed" desc="Xác Nhận Đóng Form ">
        if (txttennhanvien.getText().equals("") ) {
            FormMain frmC = new FormMain();
            frmC.setVisible(true);
            dispose();
        }else{
//        int x = JOptionPane.showConfirmDialog(this, "Bạn muốn quay lại?","Thông báo",JOptionPane.YES_NO_OPTION);
            Object[] options = {"Yes", "No"};
            int x = JOptionPane.showOptionDialog(null, "Bạn có muốn quay lại?","Thông báo",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon2, options, options[1]);
            if (x == JOptionPane.YES_OPTION) {
                  FormMain frmC = new FormMain();
                  frmC.setVisible(true);
                  dispose();
             }
           }
        //</editor-fold>
    }//GEN-LAST:event_btngetbackActionPerformed

    private void btnActiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActiveActionPerformed
        try {
            ChucNang cn = new ChucNang();
            String ma = txtmanhanvien.getText();
            getModel();
            int a = tablenhanvien.getSelectedRow();
            String staffStatus = tablenhanvien.getValueAt(a, 4).toString();
            if(staffStatus.equals("√")) JOptionPane.showMessageDialog(null, "Nhân Viên Này Đang Hoạt Động!!!"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            else if (!ma.equals("")) {
                cn.ActiveNhanVien(ma,model);
            }else JOptionPane.showMessageDialog(null, "Vui Lòng Chọn Nhân Viên Để Active"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            clear();
//            cn.showNhanVien(model);
        } catch (SQLException ex) {
            Logger.getLogger(NhanSu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnActiveActionPerformed

    private void txttennhanvienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttennhanvienKeyPressed
        char c = evt.getKeyChar();
        if(Character.isLetter(c) || Character.isWhitespace(c) || Character.isISOControl(c)){
            txttennhanvien.setEditable(true);
        }else txttennhanvien.setEditable(false);
    }//GEN-LAST:event_txttennhanvienKeyPressed

    private void txttimkiemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttimkiemKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttimkiemKeyPressed

    private void btnUpdateHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateHistoryActionPerformed
        try {
             new FormPositionChangeDetails1(this,true).show();
//            dispose();
        } catch (Exception e) {
        }

    }//GEN-LAST:event_btnUpdateHistoryActionPerformed

    private void btnExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportActionPerformed
        getModel();
        new Exporter().exportExcel(model);
    }//GEN-LAST:event_btnExportActionPerformed

    private void btnStaffDetailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStaffDetailActionPerformed
        try
        {
            getModel();
            ChucNang cn = new ChucNang();
            int a = tablenhanvien.getSelectedRow();
            String staffID = model.getValueAt(a, 0).toString();
            ArrayList<String> staff = cn.getStaff(staffID);
            DialogStaffDetail dsd = new DialogStaffDetail(this, true);
            dsd.setStaff(staff);
            dsd.show();
            
        } catch (SQLException ex)
        {
            Logger.getLogger(NhanSu.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex)
        {
            Logger.getLogger(NhanSu.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception e){
            if(tablenhanvien.getSelectedRow() == -1)
                JOptionPane.showMessageDialog(null, "Vui Lòng Chọn Nhân Viên!!!"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
        }
    }//GEN-LAST:event_btnStaffDetailActionPerformed
    public void clear(){
        //txtmanhanvien.disable();
        txtmanhanvien.setText("");
        txttennhanvien.setText(""); 
        txtsdt.setText(""); 
        txtdiachi.setText(""); 
        cboChucVu.setSelectedItem("");
        txtngaysinh.setDate(null);
        txtBonus.setText("");
        //cboChucVu.disable();
    }
    public void disable(){
        txtmanhanvien.setEditable(false);
//        txttennhanvien.disable();
//        txtsdt.disable();
//        txtdiachi.disable();
//        cboChucVu.disable();
//        txtngaysinh.disable();
    }
    public void enable(){
        txtmanhanvien.enable();
        txtngaysinh.enable(true);
        txttennhanvien.enable();
        txtsdt.enable();
        txtdiachi.enable();
        cboChucVu.enable();
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NhanSu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NhanSu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NhanSu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NhanSu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try
                {
                    new NhanSu().setVisible(true);
                } catch (SQLException ex)
                {
                    Logger.getLogger(NhanSu.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActive;
    private javax.swing.JButton btnAddNew;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnExport;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnStaffDetail;
    private javax.swing.JButton btnTimKiem;
    private javax.swing.JButton btnUpdateHistory;
    private javax.swing.JButton btngetback;
    private javax.swing.JButton btnresetpassword;
    private javax.swing.JComboBox<String> cboChucVu;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton rbtnNam;
    private javax.swing.JRadioButton rbtnNu;
    private javax.swing.ButtonGroup rbtngioitinh;
    private javax.swing.JTable tablenhanvien;
    private javax.swing.JTextField txtBonus;
    private javax.swing.JTextField txtdiachi;
    private javax.swing.JTextField txtmanhanvien;
    private com.toedter.calendar.JDateChooser txtngaysinh;
    private javax.swing.JTextField txtsdt;
    private javax.swing.JTextField txttennhanvien;
    private javax.swing.JTextField txttimkiem;
    // End of variables declaration//GEN-END:variables
}
