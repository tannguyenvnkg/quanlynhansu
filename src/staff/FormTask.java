/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff;

import leader.*;
import com.microsoft.sqlserver.jdbc.StringUtils;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import main.ChucNang;
import main.Design;
import main.FormMain;
import main.NhanVien;


/**
 *
 * @author tanng
 */
public class FormTask extends javax.swing.JFrame {

    //<editor-fold defaultstate="collapsed" desc=" Khởi Tạo Model Table ">
    public DefaultTableModel model = null;
    public DefaultTableModel getModel(){
        return model = (DefaultTableModel)tbTask.getModel();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Khởi Tạo Model combobox ">
    public DefaultComboBoxModel comboBoxModel = null;
    public DefaultComboBoxModel getComboBoxModel(){
        return comboBoxModel = (DefaultComboBoxModel)cboProject.getModel();
    }
    //</editor-fold>
    private final static int FINISH_TASK = 1;
    private final static int CANCEL_TASK = 0;
    Icon icon1 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/error-50.png"));
//    Icon icon2 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/checkmark-50.png"));
    /**
     * Creates new form FormTaskList
     */
    public FormTask() throws SQLException {
        initComponents();
        getModel();
        getComboBoxModel();
        ChucNang cn = new ChucNang();
        tbTask.setDefaultEditor(Object.class , null); // không được phép sửa trên jtable
        tbTask.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // chỉ được phép chọn 1 dòng trong Jtable
        cn.showComboboxProject(comboBoxModel); // show danh sách dự án vào combobox
        showTaskWithComboxProjectFormStaff(); // show công việc tại combobox dự án được chọn
        design();
        if(NhanVien.getInstance().getMachucvu() != 4) {
            txtTaskDescription.setEditable(false);
            btnSendReport.setVisible(false);
        } // tắt sửa đổi textbox báo cáo
    }
    private class WindowEventHandler extends WindowAdapter {
                public void windowClosing(WindowEvent evt) {                  
                        FormMain fm = new FormMain();
                        fm.show();
                        dispose();      
  }
}
    private void design(){
        // <editor-fold defaultstate="collapsed" desc="Design"> 
        //=============== Closebutton =======================
        this.addWindowListener(new WindowEventHandler());
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
         //============== bảng màu =========================
        /*   #feffde = 254, 255, 222 = kem sữa.
              #ddffbc =  221, 255, 188 = xanh nhạt 1.
              #91c788 = 145, 199, 136 = xanh đậm 1 .
              #52734d = 82, 115, 77 = xanh đậm 2.  */
        Color tbbackground = new Color(254, 255, 222);
        Color tbforeground = new Color(0,0,0);
        Color tbselectionbackground = new Color(145, 199, 136);
        Color tbselectionforeground = new Color(255,0,0);
         //============== header Input =======================
        JTableHeader header = tbTask.getTableHeader();
        header.setForeground(Color.black);
        header.setBackground(Color.RED);
        header.setFont(new Font("Times New Roman", Font.BOLD , 20));
        tbTask.setFont(new Font("Times New Roman", Font.PLAIN , 18));
        
        tbTask.setBackground(tbbackground);
        tbTask.setForeground(tbforeground);
        tbTask.setSelectionBackground(tbselectionbackground);
        tbTask.setSelectionForeground(tbselectionforeground);
        //============== header Input =======================
        new Design().resizeHeaderLong(tbTask);
        tbTask.getColumnModel().getColumn(0).setCellRenderer(new Design.AlignRenderer());
        tbTask.getColumnModel().getColumn(1).setCellRenderer(new Design.AlignRenderer());
        tbTask.getColumnModel().getColumn(2).setCellRenderer(new Design.AlignRenderer());
        tbTask.getColumnModel().getColumn(3).setCellRenderer(new Design.AlignRenderer());
        tbTask.getColumnModel().getColumn(4).setCellRenderer(new Design.AlignRenderer());
        tbTask.getColumnModel().getColumn(0).setPreferredWidth(150);
        tbTask.getColumnModel().getColumn(1).setPreferredWidth(180);
        tbTask.getColumnModel().getColumn(2).setPreferredWidth(180);
        tbTask.getColumnModel().getColumn(3).setPreferredWidth(196);
        tbTask.getColumnModel().getColumn(4).setPreferredWidth(200);
//        tbTask.getColumnModel().getColumn(1).setWidth(70);
        // </editor-fold>
    }
    private void showTaskWithComboxProjectFormStaff() throws SQLException{  // show công việc tại combobox dự án được chọn
        try
        {
            getModel();
            ChucNang cn = new ChucNang();
            String projectName = cboProject.getSelectedItem().toString();
            cn.showTaskInFormStaff(model,projectName);
        } catch (Exception e)
        {
            //JOptionPane.showMessageDialog(null, "Hiện Tại Bạn Chưa Có Công Việc"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }
        
    }
    
    private boolean checkTask(int command){ // nếu trả về true thì sẽ update Task
        getModel();
        int a = tbTask.getSelectedRow();
        String staffName = model.getValueAt(a, 1).toString();
        String statusTask = model.getValueAt(a, 4).toString();
        if(command == 1){
            if(staffName.equals("")) {
                JOptionPane.showMessageDialog(null, "Vẫn Chưa Có Nhân Viên Cho Công Việc Này"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
                return false;
            }else if(txtTaskDescription.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Nhân Viên Chưa Gửi Báo Cáo Cho Công Việc Này"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
                return false;
            }
        }else if(command == 0){
            if(statusTask.equals("Hoàn Thành")){
                JOptionPane.showMessageDialog(null, "Công Việc Này Đã Được Hoàn Thành"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
                return false;
            }
        }
        return true;
    }
    private void updateTask(int command){
        try
        {
            if(checkTask(command)){
                ChucNang cn = new ChucNang();
                getModel();
                int a = tbTask.getSelectedRow();
                String taskID = model.getValueAt(a, 0).toString();
                String projectName = cboProject.getSelectedItem().toString();
                cn.updateTask(projectName,taskID,command);
                showTaskWithComboxProjectFormStaff();
            }
        } catch (SQLException ex)
        {
            Logger.getLogger(FormTask.class.getName()).log(Level.SEVERE, null, ex);
        }catch (Exception e){
            if(tbTask.getSelectedRow() == -1) 
//                MessageBox.showMessageBox().showError("bạn chưa chọn công việc");
            JOptionPane.showMessageDialog(null, "Bạn Chưa Chọn Công Việc"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbTask = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        cboProject = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtTaskReport = new javax.swing.JTextArea();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtTaskDescription = new javax.swing.JTextArea();
        jPanel6 = new javax.swing.JPanel();
        btnSendReport = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Danh Sách Công Việc");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 255, 204));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("Danh Sách Công Việc");

        jPanel3.setBackground(new java.awt.Color(204, 255, 204));

        tbTask.setBackground(new java.awt.Color(254, 255, 222));
        tbTask.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã Công Việc", "Ngày Bắt Đầu", "Ngày Kết Thúc", "Trạng Thái Công Việc", "Trạng Thái Báo Cáo"
            }
        ));
        tbTask.setFillsViewportHeight(true);
        tbTask.getTableHeader().setReorderingAllowed(false);
        tbTask.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbTaskMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbTask);

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));

        cboProject.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        cboProject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboProjectActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setText("Chọn Dự Án");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(cboProject, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cboProject, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel2))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 12, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 912, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(204, 255, 204));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 255));
        jLabel3.setText("Nội Dung Báo Cáo");

        txtTaskReport.setColumns(20);
        txtTaskReport.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        txtTaskReport.setLineWrap(true);
        txtTaskReport.setRows(5);
        txtTaskReport.setWrapStyleWord(true);
        jScrollPane2.setViewportView(txtTaskReport);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 457, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addGap(0, 213, Short.MAX_VALUE)))
                    .addContainerGap()))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 272, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel3)
                    .addGap(18, 18, 18)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel5.setBackground(new java.awt.Color(204, 255, 204));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 255));
        jLabel4.setText("Nội Dung Công Việc");

        txtTaskDescription.setEditable(false);
        txtTaskDescription.setColumns(20);
        txtTaskDescription.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        txtTaskDescription.setLineWrap(true);
        txtTaskDescription.setRows(5);
        txtTaskDescription.setWrapStyleWord(true);
        jScrollPane3.setViewportView(txtTaskDescription);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addContainerGap(236, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addContainerGap(231, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addGap(59, 59, 59)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel6.setBackground(new java.awt.Color(204, 255, 204));

        btnSendReport.setBackground(new java.awt.Color(255, 255, 255));
        btnSendReport.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnSendReport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/undo-30.png"))); // NOI18N
        btnSendReport.setText("Gửi Báo Cáo");
        btnSendReport.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnSendReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendReportActionPerformed(evt);
            }
        });

        btnExit.setBackground(new java.awt.Color(255, 255, 255));
        btnExit.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        btnExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/undo-30.png"))); // NOI18N
        btnExit.setText("Trở Lại");
        btnExit.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnSendReport)
                .addGap(18, 18, 18)
                .addComponent(btnExit)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSendReport, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(196, 196, 196)
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        new FormMain().show();
        dispose();
    }//GEN-LAST:event_btnExitActionPerformed

    private void cboProjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboProjectActionPerformed
        try
        {
            txtTaskDescription.setText("");
            txtTaskReport.setText("");
            showTaskWithComboxProjectFormStaff();
        } catch (SQLException ex)
        {
            Logger.getLogger(FormTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_cboProjectActionPerformed
// test sourcetree
    private void tbTaskMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbTaskMouseClicked
        try
        {
            ChucNang cn = new ChucNang();
            getModel();
            int a = tbTask.getSelectedRow();
            String taskID = model.getValueAt(a, 0).toString();
            String projectName = cboProject.getSelectedItem().toString();
            
            txtTaskDescription.setText("");
            if(cn.checkReportStatus(taskID,projectName)) txtTaskReport.setEditable(false); // tắt báo cáo khi đã gửi
            else txtTaskReport.setEditable(true);
            String taskDescription = cn.getTaskDescription(taskID,projectName);
            String taskReport = cn.getTaskReport(taskID,projectName);
            txtTaskDescription.setText(taskDescription);
            if (!StringUtils.isEmpty(taskReport)) txtTaskReport.setText(taskReport);
            else txtTaskReport.setText("");
            //String taskStatus = model.getValueAt(a, 4).toString();
//            if(taskStatus.equals("Đã gửi")) txtTaskReport.setEditable(false);
//            else txtTaskReport.setEditable(true);
        } catch (SQLException ex)
        {
            Logger.getLogger(FormTask.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception e){
            //e.printStackTrace();
        }
    }//GEN-LAST:event_tbTaskMouseClicked

    private void btnSendReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendReportActionPerformed
        try
        {
            ChucNang cn = new ChucNang();
            getModel();
            int a = tbTask.getSelectedRow();
            String taskID = model.getValueAt(a, 0).toString();
            String projectName = cboProject.getSelectedItem().toString();
            String taskReport = txtTaskReport.getText();
            String taskStatus = model.getValueAt(a, 4).toString();
            if(taskReport.equals("")) 
                JOptionPane.showMessageDialog(null, "Nội Dung Báo Cáo Không Được Để Trống"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
            else if(taskStatus.equals("Đã gửi")) JOptionPane.showMessageDialog(null, "Bạn Đã Báo Cáo Cho Công Việc Này"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
            else if(!cn.checkReportStatus(taskID, projectName)){
                Object[] options = {"Yes", "No"}; // hỏi yes no để xác nhận xóa
                int dialogresult = JOptionPane.showOptionDialog(null, "Xác nhân Gửi Báo Cáo?"," Warning ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon1, options, options[1]);
                if (dialogresult == 0){
                    cn.sendReport(taskID,projectName,taskReport);
                    showTaskWithComboxProjectFormStaff();
                    cboProject.setSelectedItem(projectName);
                }
            }
            else 
            JOptionPane.showMessageDialog(null, "Bạn Đã Gửi Báo Cáo Cho Công Việc Này"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
        } catch (SQLException ex)
        {
            Logger.getLogger(FormTask.class.getName()).log(Level.SEVERE, null, ex);
        }catch (Exception e){
            if(tbTask.getSelectedRow() == -1) 
                JOptionPane.showMessageDialog(null, "Vui Lòng Chọn Công Việc"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }
    }//GEN-LAST:event_btnSendReportActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(FormTask.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(FormTask.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex)
        {
            java.util.logging.Logger.getLogger(FormTask.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(FormTask.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try
                {
                    new FormTask().setVisible(true);
                } catch (SQLException ex)
                {
                    Logger.getLogger(FormTask.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnSendReport;
    private javax.swing.JComboBox<String> cboProject;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tbTask;
    private javax.swing.JTextArea txtTaskDescription;
    private javax.swing.JTextArea txtTaskReport;
    // End of variables declaration//GEN-END:variables
}
