
package main;

import java.awt.Image;
import java.awt.Toolkit;
import personnel.NhanSu;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import project.FormProjectManager;
import room.FormRoom;
import login.profile.FormProfile;
import login.profile.FormLogin;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import project.FormProjectAdmin;
import leader.FormCreateTask1;
import leader.FormTaskList;
import personnel.FormSalary;
import project.FormCreateProject1;
import staff.FormTask;

public class FormMain extends javax.swing.JFrame {

    Icon icon1 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/denied-50.png"));
    Icon icon2 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/help-50.png"));
    ImageIcon icon3 = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/image/avartar/logo.png")));
    public FormMain() {
        initComponents();
        scaleImage();
        this.pack();
        this.addWindowListener(new WindowEventHandler_Main());
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        hidePosition();
        this.pack();
    }
            
class WindowEventHandler_Main extends WindowAdapter {
  public void windowClosing(WindowEvent evt) {

      Object[] options = {"Yes", "No"};
      int x = JOptionPane.showOptionDialog(null, "Bạn có muốn đăng xuất?"," Warning ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon2, options, options[1]);
      if (x == JOptionPane.YES_OPTION) {
            FormLogin fl = new FormLogin();
            fl.show();
            dispose();
        }
  }
}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator4 = new javax.swing.JSeparator();
        jMenuItem8 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        lblogo = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        MnTaiKhoan = new javax.swing.JMenu();
        MnItThongTin = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        MnItLogout = new javax.swing.JMenuItem();
        MnNhanVien = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        MnLeader = new javax.swing.JMenu();
        miCreateTask = new javax.swing.JMenuItem();
        miTaskList = new javax.swing.JMenuItem();
        MnPM = new javax.swing.JMenu();
        miCreateProject = new javax.swing.JMenuItem();
        miProjectList = new javax.swing.JMenuItem();
        MnAdmin = new javax.swing.JMenu();
        MnItNS = new javax.swing.JMenuItem();
        btnQuanLyPhong = new javax.swing.JMenuItem();
        miSalary = new javax.swing.JMenuItem();

        jMenuItem8.setText("jMenuItem8");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Trang Chủ");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 255, 204));

        lblogo.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(67, 67, 67)
                .addComponent(lblogo, javax.swing.GroupLayout.PREFERRED_SIZE, 714, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(82, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(lblogo, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(113, Short.MAX_VALUE))
        );

        jMenuBar1.setBackground(new java.awt.Color(255, 255, 255));

        MnTaiKhoan.setBackground(new java.awt.Color(255, 255, 255));
        MnTaiKhoan.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        MnTaiKhoan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Avartar/icons8-name-tag-50.png"))); // NOI18N
        MnTaiKhoan.setText("Tài Khoản  ");
        MnTaiKhoan.setActionCommand(" Tài Khoản  ");
        MnTaiKhoan.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N

        MnItThongTin.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        MnItThongTin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-admin-settings-male-30.png"))); // NOI18N
        MnItThongTin.setText("Hồ Sơ");
        MnItThongTin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnItThongTinActionPerformed(evt);
            }
        });
        MnTaiKhoan.add(MnItThongTin);
        MnTaiKhoan.add(jSeparator1);

        MnItLogout.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        MnItLogout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Edit/icons8-sign-out-30.png"))); // NOI18N
        MnItLogout.setText("Đăng Xuất");
        MnItLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnItLogoutActionPerformed(evt);
            }
        });
        MnTaiKhoan.add(MnItLogout);

        jMenuBar1.add(MnTaiKhoan);

        MnNhanVien.setBackground(new java.awt.Color(255, 255, 255));
        MnNhanVien.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, java.awt.Color.white, null, null));
        MnNhanVien.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Avartar/icons8-account-50.png"))); // NOI18N
        MnNhanVien.setText(" Nhân Viên  ");
        MnNhanVien.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N

        jMenuItem3.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/work-from-home-30.png"))); // NOI18N
        jMenuItem3.setText("Công Việc");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        MnNhanVien.add(jMenuItem3);

        jMenuBar1.add(MnNhanVien);

        MnLeader.setBackground(new java.awt.Color(255, 255, 255));
        MnLeader.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        MnLeader.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Avartar/icons8-contact-details-50.png"))); // NOI18N
        MnLeader.setText("Trưởng Phòng ");
        MnLeader.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        MnLeader.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                MnLeaderMousePressed(evt);
            }
        });

        miCreateTask.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        miCreateTask.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/edit-file-30.png"))); // NOI18N
        miCreateTask.setText("Tạo Công Việc");
        miCreateTask.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miCreateTaskActionPerformed(evt);
            }
        });
        MnLeader.add(miCreateTask);

        miTaskList.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        miTaskList.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/list-30.png"))); // NOI18N
        miTaskList.setText("Danh Sách Công Việc");
        miTaskList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miTaskListActionPerformed(evt);
            }
        });
        MnLeader.add(miTaskList);

        jMenuBar1.add(MnLeader);

        MnPM.setBackground(new java.awt.Color(255, 255, 255));
        MnPM.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        MnPM.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Avartar/icons8-walter-white-50.png"))); // NOI18N
        MnPM.setText("Quản Lý Dự Án ");
        MnPM.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        MnPM.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                MnPMMousePressed(evt);
            }
        });

        miCreateProject.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        miCreateProject.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/edit-file-30.png"))); // NOI18N
        miCreateProject.setText("Tạo Dự Án");
        miCreateProject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miCreateProjectActionPerformed(evt);
            }
        });
        MnPM.add(miCreateProject);

        miProjectList.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        miProjectList.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/list-30.png"))); // NOI18N
        miProjectList.setText("Danh Sách Dự Án");
        miProjectList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miProjectListActionPerformed(evt);
            }
        });
        MnPM.add(miProjectList);

        jMenuBar1.add(MnPM);

        MnAdmin.setBackground(new java.awt.Color(255, 255, 255));
        MnAdmin.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        MnAdmin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Favorites/Avartar/icons8-vip-50.png"))); // NOI18N
        MnAdmin.setText("Nhân Sự ");
        MnAdmin.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        MnAdmin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MnAdminMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                MnAdminMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                MnAdminMousePressed(evt);
            }
        });

        MnItNS.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        MnItNS.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/add-user-female-30.png"))); // NOI18N
        MnItNS.setText("Quản Lý Nhân Sự");
        MnItNS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnItNSActionPerformed(evt);
            }
        });
        MnAdmin.add(MnItNS);

        btnQuanLyPhong.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnQuanLyPhong.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit/add-user-group-30.png"))); // NOI18N
        btnQuanLyPhong.setText("Quản Lý Phòng");
        btnQuanLyPhong.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuanLyPhongActionPerformed(evt);
            }
        });
        MnAdmin.add(btnQuanLyPhong);

        miSalary.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        miSalary.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/folder/icons8-pie-chart-30.png"))); // NOI18N
        miSalary.setText("Thống Kê");
        miSalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSalaryActionPerformed(evt);
            }
        });
        MnAdmin.add(miSalary);

        jMenuBar1.add(MnAdmin);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    private void hidePosition(){
        if(NhanVien.getInstance().getMachucvu()== 1) miCreateProject.setVisible(false);
        if(NhanVien.getInstance().getMachucvu() == 1 || NhanVien.getInstance().getMachucvu() == 2) miCreateTask.setVisible(false);
        if(NhanVien.getInstance().getMachucvu()== 2)MnAdmin.setVisible(false);
        if(NhanVien.getInstance().getMachucvu()== 3){
            MnAdmin.setVisible(false);
            MnPM.setVisible(false);
            MnNhanVien.setVisible(false);
        }
        if(NhanVien.getInstance().getMachucvu()== 4){
            MnAdmin.setVisible(false);
            MnPM.setVisible(false);
            MnLeader.setVisible(false);
        }
    }
    public void scaleImage(){
        Image img = icon3.getImage();
        Image imgScale = img.getScaledInstance(lblogo.getWidth(), lblogo.getHeight(),Image.SCALE_SMOOTH);
        ImageIcon scaleIcon = new ImageIcon(imgScale);
        lblogo.setIcon(scaleIcon);
    }
    private void MnItThongTinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnItThongTinActionPerformed
        try {
            FormProfile frmProfile = new FormProfile();
            frmProfile.showProfile();
            frmProfile.show();
            dispose();
            
        } catch (SQLException ex) {
            Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex)
        {
            Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_MnItThongTinActionPerformed

    private void MnItLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnItLogoutActionPerformed
            //<editor-fold defaultstate="collapsed" desc=" Xác Nhận Đóng Form "> 
            Object[] options = {"Yes", "No"};
            int x = JOptionPane.showOptionDialog(null, "Bạn có muốn đăng xuất?","Thông báo",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon2, options, options[1]);
           if (x == JOptionPane.YES_OPTION) {
            FormLogin fl = new FormLogin();
            fl.show();
            dispose();
        }
        //</editor-fold>
    }//GEN-LAST:event_MnItLogoutActionPerformed

    private void MnItNSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnItNSActionPerformed
        try{
            new NhanSu().show();
            dispose();
        } catch (SQLException ex){
            Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_MnItNSActionPerformed

    private void miProjectListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miProjectListActionPerformed
        try{
            if(NhanVien.getInstance().getMachucvu() == 2){
            FormProjectManager frmProject = new FormProjectManager();
            frmProject.show();
            }else if(NhanVien.getInstance().getMachucvu() == 1) {
                FormProjectAdmin frmProject = new FormProjectAdmin();
                frmProject.show();
            }
        dispose();
        } catch (SQLException ex) {
            Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_miProjectListActionPerformed

    private void btnQuanLyPhongActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuanLyPhongActionPerformed
        try {
            FormRoom frmPhong = new FormRoom();
            frmPhong.show();
            dispose();
        } catch (SQLException ex) {
            Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }//GEN-LAST:event_btnQuanLyPhongActionPerformed

    private void MnAdminMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MnAdminMouseClicked
        
    }//GEN-LAST:event_MnAdminMouseClicked

    private void MnAdminMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MnAdminMouseEntered
       
    }//GEN-LAST:event_MnAdminMouseEntered

    private void MnAdminMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MnAdminMousePressed
        if(NhanVien.getInstance().getMachucvu()>1)
           JOptionPane.showMessageDialog(null, " Bạn không có quyền truy cập "," Warning ",JOptionPane.INFORMATION_MESSAGE,icon1);
    }//GEN-LAST:event_MnAdminMousePressed

    private void miCreateProjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miCreateProjectActionPerformed
        try {
            FormCreateProject1 fcp;
            fcp = new FormCreateProject1(this,true);
            fcp.show();
//            dispose();
        } catch (SQLException ex) {
            Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_miCreateProjectActionPerformed

    private void MnPMMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MnPMMousePressed
        if(NhanVien.getInstance().getMachucvu()>2)
           JOptionPane.showMessageDialog(null, " Bạn không có quyền truy cập "," Warning ",JOptionPane.INFORMATION_MESSAGE,icon1);
    }//GEN-LAST:event_MnPMMousePressed

    private void MnLeaderMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MnLeaderMousePressed
          if(NhanVien.getInstance().getMachucvu()>3)
           JOptionPane.showMessageDialog(null, " Bạn không có quyền truy cập "," Warning ",JOptionPane.INFORMATION_MESSAGE,icon1);
    }//GEN-LAST:event_MnLeaderMousePressed

    private void miCreateTaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miCreateTaskActionPerformed
        try
        {
            new FormCreateTask1(this,true).setVisible(true);
//            dispose();
        } catch (SQLException ex)
        {
            Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_miCreateTaskActionPerformed

    private void miTaskListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miTaskListActionPerformed
        try
        {
            new FormTaskList().setVisible(true);
            dispose();
        } catch (SQLException ex)
        {
            Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_miTaskListActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        try
        {
            new FormTask().show();
            dispose();
        } catch (SQLException ex)
        {
            Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void miSalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSalaryActionPerformed
        new FormSalary().show();
        dispose();
    }//GEN-LAST:event_miSalaryActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormMain().setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu MnAdmin;
    private javax.swing.JMenuItem MnItLogout;
    private javax.swing.JMenuItem MnItNS;
    private javax.swing.JMenuItem MnItThongTin;
    private javax.swing.JMenu MnLeader;
    private javax.swing.JMenu MnNhanVien;
    private javax.swing.JMenu MnPM;
    private javax.swing.JMenu MnTaiKhoan;
    private javax.swing.JMenuItem btnQuanLyPhong;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lblogo;
    private javax.swing.JMenuItem miCreateProject;
    private javax.swing.JMenuItem miCreateTask;
    private javax.swing.JMenuItem miProjectList;
    private javax.swing.JMenuItem miSalary;
    private javax.swing.JMenuItem miTaskList;
    // End of variables declaration//GEN-END:variables
}
