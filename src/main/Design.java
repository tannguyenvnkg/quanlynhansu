/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

/**
 *
 * @author Admin
 */
//<editor-fold defaultstate="collapsed" desc="Khai Báo">
/*
Sử dụng renderer cho từng cột

            myTable.getColumnModel().getColumn(0).setCellRenderer(new MyTableRenderer.OutputRenderer());
            myTable.getColumnModel().getColumn(1).setCellRenderer(new MyTableRenderer.BgRenderer());
Sử dụng renderer cho từng kiểu dữ liệu

    Áp dụng renderer cho tất cả các cột có cùng kiểu dữ liệu:

            myTable.setDefaultRenderer(Number.class, new MyTableRenderer.CurrencyRenderer());
            myTable.setDefaultRenderer(Boolean.class, new MyTableRenderer.ImageRenderer());
*/
//</editor-fold>
public class Design {
//    Căn lề cho cột
    public static class AlignRenderer extends DefaultTableCellRenderer {
 
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        // align center
        setHorizontalAlignment(SwingConstants.CENTER);
        // align left
        //setHorizontalAlignment(SwingConstants.LEFT);
        // align right
        //setHorizontalAlignment(SwingConstants.RIGHT);
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        return this;
    }
}
//   Chèn ảnh vào ô của bảng
    public static class ImageRenderer extends DefaultTableCellRenderer {
 
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        boolean bool = Boolean.parseBoolean(value.toString());
        Image img = null;
        if(bool) {
            img = getToolkit().getImage(getClass().getResource("/image/edit/ok-30.png"));
        } else {
            img = getToolkit().getImage(getClass().getResource("/image/edit/cancel-30.png"));
        }
        setSize(16, 16);
        setHorizontalAlignment(SwingConstants.CENTER);
        setIcon(new ImageIcon(img));
        super.getTableCellRendererComponent(table, "", isSelected, hasFocus, row, column);
        return this;
    }
}
//   Tạo màu nền cho các ô trong bảng
public static class BgRenderer extends DefaultTableCellRenderer {
 
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JPanel pn = new JPanel(new BorderLayout());
        JLabel lb = new JLabel();
        pn.add(lb, BorderLayout.CENTER);
        lb.setHorizontalAlignment(SwingConstants.CENTER);
        lb.setText(value.toString());
        lb.setForeground(Color.BLUE);
        pn.setBackground(Color.RED);
 
        super.getTableCellRendererComponent(table, value, isSelected,
                hasFocus, row, column);
        return pn;
    }
}
//   Căn lề cho header + resize
     public void resizeHeaderLong(JTable table) {
         int c = 0;
         c = table.getColumnCount();
         if (c >=4) {
             table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
             ((DefaultTableCellRenderer)table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
             table.setPreferredScrollableViewportSize(table.getPreferredSize());
             table.setRowHeight(30);
             table.setShowGrid(true);
             for(int i=0;i<table.getColumnCount();i++){
              DefaultTableColumnModel colModel = (DefaultTableColumnModel) table.getColumnModel();
             TableColumn col = colModel.getColumn(i);
              int width = 0;

             TableCellRenderer renderer = col.getHeaderRenderer();
              if (renderer == null) {
               renderer = table.getTableHeader().getDefaultRenderer();
             }
            Component comp = renderer.getTableCellRendererComponent(table, col.getHeaderValue(), false, false, 0, 0);
              width = comp.getPreferredSize().width;
              col.setPreferredWidth(width+10);
             }
         }
         else{
             ((DefaultTableCellRenderer)table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
              table.setRowHeight(30);
              table.setShowGrid(true);
         }
  }
//      public void resizeHeaderShort(JTable table) {
//////    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//    ((DefaultTableCellRenderer)table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
//    table.setRowHeight(30);
//    table.setShowGrid(true);
//////    for(int i=0;i<table.getColumnCount();i++){
//////      DefaultTableColumnModel colModel = (DefaultTableColumnModel) table.getColumnModel();
//////      TableColumn col = colModel.getColumn(i);
//////      int width = 0;
//////
//////      TableCellRenderer renderer = col.getHeaderRenderer();
//////      if (renderer == null) {
//////        renderer = table.getTableHeader().getDefaultRenderer();
//////      }
//////      Component comp = renderer.getTableCellRendererComponent(table, col.getHeaderValue(), false, false, 0, 0);
//////      width = comp.getPreferredSize().width;
//////      col.setPreferredWidth(width+15);
//////    }
//  }
}

