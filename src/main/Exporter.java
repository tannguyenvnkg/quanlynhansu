package main;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tanng
 */
public class Exporter {
     Icon icon1 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/checkmark-50.png"));
     Icon icon2 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/error-50.png"));
    public void exportExcel(DefaultTableModel model) {
        JFileChooser chooser = new JFileChooser();
        int i = chooser.showSaveDialog(chooser);
        if (i == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            try {
                FileWriter out = new FileWriter(file + ".csv");
                BufferedWriter bwrite = new BufferedWriter(out);
                // ten Cot
                for (int j = 0; j < model.getColumnCount(); j++) {
                    bwrite.write(model.getColumnName(j) + "\t");
                }
                bwrite.write("\n");
                // Lay du lieu dong
                for (int j = 0; j < model.getRowCount(); j++) {
                    for (int k = 0; k < model.getColumnCount(); k++) {
                        bwrite.write(model.getValueAt(j, k) + "\t");
                    }
                bwrite.write("\n");
                }
                bwrite.close();
//                JOptionPane.showMessageDialog(null, "Xuất File Excel Thành Công!!!");
                JOptionPane.showMessageDialog(null, "Xuất File Excel Thành Công!!!"," Thông báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
            }catch (Exception e2) {
//             JOptionPane.showMessageDialog(null, "Lỗi khi lưu file!");
             JOptionPane.showMessageDialog(null, "Lỗi khi lưu file!"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            }
        }
   }
}
