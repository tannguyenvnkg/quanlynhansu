/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//<editor-fold defaultstate="collapsed" desc="Ghi Chú">
//</editor-fold>
package main;
import com.microsoft.sqlserver.jdbc.StringUtils;
import database.FormRestoreAndBackupDatabase1;
import database.RestoreDatabase;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.ParseException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import sun.security.rsa.RSACore;
//SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
/**
 *
 * @author tanng
 */
public class ChucNang extends Database{
    DefaultTableModel modeltemp = null;
    Icon icon1 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/checkmark-50.png"));
    Icon icon2 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/error-50.png"));
    Icon icon3 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/checked-user-male-50.png"));
    Icon icon4 = new javax.swing.ImageIcon(getClass().getResource("/image/edit/help-50.png"));
    //<editor-fold defaultstate="collapsed" desc="Form Login">
    public  boolean login(String user, String pass) throws SQLException, NoSuchAlgorithmException{
        connect(); // kết nối database
        pass = MD5(pass);
        String query = "select * from NHANVIEN where manhanvien= '"+user+"' and matkhau = '"+pass+"'";
        ResultSet rs = stmt.executeQuery(query);
        ResultSet rs1 = getTHOIGIANNHANVIEC(user);
        if(rs.next() && rs1.next()){
            NhanVien.getInstance().LuuNhanVien(rs,rs1);
            return true;
        }
        else return false;
    }
    public boolean checktrangthai() {
        return NhanVien.getInstance().getTrangthai();
    }
    public boolean FirstLogin(String user, String pass) throws NoSuchAlgorithmException{
        user = MD5(user);
        if(user.equals(NhanVien.getInstance().getMatkhau())) return true;
        return false;
    }
    
    public boolean checkUser(String staffID) throws SQLException{ // nếu user có tồn tại thì trả về true
        connect();
        String query = "select * from nhanvien where manhanvien = "+staffID+"";
        ResultSet rs = stmt.executeQuery(query);
        if(rs.next()) return true;
        return false;
    }
    private int getNumberOfLogin(String staffID) throws SQLException{ // lấy số lần đăng nhập sai
        connect();
        String query = "select solandangnhap from nhanvien where manhanvien = '"+staffID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next())
        {            
            int a = rs.getInt(1);
            if(a==4) JOptionPane.showMessageDialog(null, "Tài Khoản Nhân Viên Đã Bị Khóa Do Nhập Sai Quá 5 Lần\nVui Lòng Liên Hệ Quản Trị Viên Để Mở Khóa"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon2);
            return a;
        }
        return -1;
    }
    private boolean checkStatusStaff(String staffID) throws SQLException{ // nếu trạng thái nhân viên là 1 thì return true
        connect();
        String query = "select trangthainhanvien from nhanvien where manhanvien = '"+staffID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()){
            return rs.getBoolean(1);
        }
        return true;
    }
    public void setNumberOfLogin(String staffID) throws SQLException{ // số lần đăng nhập sai cộng thêm 1
        if(checkStatusStaff(staffID)){
            connect();
            int numberOfLogin = getNumberOfLogin(staffID) + 1;
            String query = "update nhanvien set solandangnhap = "+numberOfLogin+" where manhanvien = '"+staffID+"'";
            stmt.execute(query);
        }
    }
    public void resetNumberOfLogin(String staffID) throws SQLException{
        connect();
        String query = "update nhanvien set solandangnhap = 0 where manhanvien = '"+staffID+"'";
        stmt.execute(query);
    }
//</editor-fold>
    //==========================================================================================================
    //<editor-fold defaultstate="collapsed" desc="show nhân viên lên jtable">
    
    public void shownhanvien_Admin(DefaultTableModel modeltemp){
        connect();
        modeltemp.setRowCount(0); // clear jtable
        String query = "select * from nhanvien \n" +
                        "order by " +
                        "trangthainhanvien desc,machucvu asc"; 
            try {
                String trangthai = "";
                String chucvu = "";
                String gioitinh = "";
                SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                
                ResultSet rs = stmt.executeQuery(query);
                while(rs.next()){
                    String ma = rs.getString("manhanvien");
                    String ten = rs.getString("tennhanvien");
                    String ngaysinh = date.format(rs.getDate("ngaysinh"));
                    String address = rs.getString("DiaChi");
                    String SDT = rs.getString("SDT");
                    if(rs.getBoolean("gioitinh")) gioitinh = "Nam"; else gioitinh = "Nữ";

                    if(rs.getBoolean("trangthainhanvien")) trangthai = "true";
                    else trangthai = "false";
                    ResultSet rs1 = getTHOIGIANNHANVIEC(ma);
                    while (rs1.next()) {                        
                        chucvu = laytenchucvu(rs1.getInt("machucvu"));
                    }
                    int bonus = rs.getInt("tienthuong1congviec");
                    Object tbData[] = {ma,ten,gioitinh/*,ngaysinh,SDT,address*/,chucvu/*,String.valueOf(bonus)*/,trangthai}; 
                    modeltemp.addRow(tbData);
                }

            } catch (SQLException ex) {
                Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
            }      
    }
    public void shownhanvien_PM(DefaultTableModel modeltemp){
        connect();
        // query get nhân viên từ database sắp sếp theo chức vụ và Active
        String query = "select * from nhanvien \n" +
                        "order by " +
                        "manhanvien asc,trangthainhanvien desc"; 
        modeltemp.setRowCount(0); // clear jtable
            try {
                String trangthai = "";
                String chucvu = "";
                String gioitinh = "";
                SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                
                ResultSet rs = stmt.executeQuery(query);
                while(rs.next()){
                    String ma = rs.getString("manhanvien");
                    String ten = rs.getString("tennhanvien");
                    String ngaysinh = date.format(rs.getDate("ngaysinh"));
                    String address = rs.getString("DiaChi");
                    String SDT = rs.getString("SDT");
                    if(rs.getBoolean("gioitinh")) gioitinh = "Nam"; else gioitinh = "Nữ";
                    //String matkhau = rs.getString("matkhau");

                    if(rs.getBoolean("trangthainhanvien")) trangthai = "Active";
                    else trangthai = "Deactive";

                    ResultSet rs1 = getTHOIGIANNHANVIEC(ma);
                    while (rs1.next()) {                        
                        chucvu = laytenchucvu(rs1.getInt("machucvu"));
                    }
                    if(chucvu.equals("Admin") || chucvu.equals("Project Manager") ) continue;
                    String tbData[] = {ma,ten,chucvu}; 
                    modeltemp.addRow(tbData);
                }

            } catch (SQLException ex) {
                Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
            }      
    }
    public void showNhanVien(DefaultTableModel model) throws SQLException{
        modeltemp = model;
        if(NhanVien.getInstance().machucvu == 1) shownhanvien_Admin(modeltemp);
        else shownhanvien_PM(modeltemp);
    }
    public String laytenchucvu(int ma) throws SQLException{
        connect();
        String query = "select tenchucvu from chucvu where machucvu = "+ma+"";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            return rs.getString(1);
        }
        return null;
     }
     // add combobox tại quản lý
    public void showcomboboxchucvu(DefaultComboBoxModel aBoxModel) throws SQLException{
        connect();
        String query = "Select * from chucvu";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {                
            aBoxModel.addElement(rs.getString("Tenchucvu"));
        }
    } 
    public void SearchNhanVienTheoMa(DefaultTableModel modeltemp,String manhanvien){
        connect();
        String query = "select * from dbo.timnhanvientheoma(N'"+manhanvien+"')";
        modeltemp.setRowCount(0);
        try {
                String trangthai = "";
                String chucvu = "";
                String gioitinh = "";
                SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                
                ResultSet rs = stmt.executeQuery(query);
                while(rs.next()){
                    String ma = rs.getString("manhanvien");
                    String ten = rs.getString("tennhanvien");
                    String ngaysinh = date.format(rs.getDate("ngaysinh"));
                    String address = rs.getString("DiaChi");
                    String SDT = rs.getString("SDT");
                    if(rs.getBoolean("gioitinh")) gioitinh = "Nam"; else gioitinh = "Nữ";
                    //String matkhau = rs.getString("matkhau");

                    if(rs.getBoolean("trangthainhanvien")) trangthai = "true";
                    else trangthai = "false";
                    int bonus = rs.getInt("tienthuong1congviec");
                    ResultSet rs1 = getTHOIGIANNHANVIEC(ma);
                    while (rs1.next()) {                        
                        chucvu = laytenchucvu(rs1.getInt("machucvu"));
                    }
                    //String tbData[] = {ma,ten,gioitinh,ngaysinh,SDT,address,chucvu,String.valueOf(bonus),trangthai}; 
                    Object tbData[] = {ma,ten,gioitinh/*,ngaysinh,SDT,address*/,chucvu/*,String.valueOf(bonus)*/,trangthai}; 
                    modeltemp.addRow(tbData);
                }

            } catch (SQLException ex) {
                Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
            }      
    }
    public void SearchNhanVienTheoTen(DefaultTableModel modeltemp,String tennhanvien){
        connect();
        String query = "select * from dbo.timnhanvientheoten(N'"+tennhanvien+"')";
        modeltemp.setRowCount(0);
        try {
                String trangthai = "";
                String chucvu = "";
                String gioitinh = "";
                SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                
                ResultSet rs = stmt.executeQuery(query);
                while(rs.next()){
                    String ma = rs.getString("manhanvien");
                    String ten = rs.getString("tennhanvien");
                    String ngaysinh = date.format(rs.getDate("ngaysinh"));
                    String address = rs.getString("DiaChi");
                    String SDT = rs.getString("SDT");
                    if(rs.getBoolean("gioitinh")) gioitinh = "Nam"; else gioitinh = "Nữ";

                    if(rs.getBoolean("trangthainhanvien")) trangthai = "true";
                    else trangthai = "false";

                    ResultSet rs1 = getTHOIGIANNHANVIEC(ma);
                    while (rs1.next()) {                        
                        chucvu = laytenchucvu(rs1.getInt("machucvu"));
                    }
                    int bonus = rs.getInt("tienthuong1congviec");
//                    String tbData[] = {ma,ten,gioitinh,ngaysinh,SDT,address,chucvu,String.valueOf(bonus),trangthai}; 
                    Object tbData[] = {ma,ten,gioitinh/*,ngaysinh,SDT,address*/,chucvu/*,String.valueOf(bonus)*/,trangthai}; 
                    modeltemp.addRow(tbData);
                }

            } catch (SQLException ex) {
                Logger.getLogger(FormMain.class.getName()).log(Level.SEVERE, null, ex);
            }      
    }
    
    public void showPositionChangeDetails(DefaultTableModel modeltemp) throws SQLException{
        connect();
        modeltemp.setRowCount(0); // clear jtable
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        String query = "select  nv.manhanvien,nv.tennhanvien,cv.tenchucvu,ngaynhancv \n" +
                        "from NHANVIEN nv join THOIGIANNHANVIEC tgnv on nv.manhanvien = tgnv.manhanvien\n" +
                        "join CHUCVU cv on nv.machucvu=cv.machucvu\n" +
                        "order by ngaynhancv desc";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            String staffID = rs.getString(1);
            String staffName = rs.getString(2);
            String positionName = rs.getString(3);
            String newPositionDay = date.format(rs.getDate(4));
            String tbData[] = {staffID,staffName,positionName,newPositionDay}; 
            modeltemp.addRow(tbData);
        }
    }
    public void showJoiningRoomDetails(DefaultTableModel modeltemp) throws SQLException{
        connect();
        modeltemp.setRowCount(0); // clear jtable
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        String query = "select nv.manhanvien,nv.tennhanvien,tenphong,ngaynhanphong,ngayroiphong\n" +
                        "from THOIGIANNHANPHONG tgnp  join NHANVIEN nv on tgnp.manhanvien = nv.manhanvien\n" +
                        "join PHONG p on tgnp.maphong = p.maphong\n" +
                        "order by ngaynhanphong desc";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            String outRoomDay = "";
            String staffID = rs.getString(1);
            String staffName = rs.getString(2);
            String roomName = rs.getString(3);
            String joinRoomDay = date.format(rs.getDate(4));
            if (!StringUtils.isEmpty(rs.getString(5))) outRoomDay = date.format(rs.getDate(5));
            String tbData[] = {staffID,staffName,roomName,joinRoomDay,outRoomDay}; 
            modeltemp.addRow(tbData);
        }
    }
    //</editor-fold>
    //==========================================================================================================
    //<editor-fold defaultstate="collapsed" desc="Đổi Pass">
    // đổi pass
    public boolean checkpass(String passString) throws NoSuchAlgorithmException{
        passString = MD5(passString);
        return passString.equals(NhanVien.getInstance().matkhau);
    }
    public boolean ChangePass(String newpass,String confirmpass) throws SQLException, NoSuchAlgorithmException{
        if(newpass.equals(confirmpass)){   //check pass mới giống nhau
            connect();
            newpass = MD5(newpass);
            String checktrungpass = NhanVien.getInstance().getManhanvien();
            checktrungpass = MD5(checktrungpass);
            if(checktrungpass.equals(newpass)){
                JOptionPane.showMessageDialog(null, "Mật Khẩu Không Được Trùng Với Tài Khoản"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
                return false;
            }
            else {
                String query = "update NHANVIEN set MatKhau = '"+newpass+"' where manhanvien = '"+NhanVien.getInstance().manhanvien+"'";
                stmt.execute(query);
                NhanVien.getInstance().setMatkhau(newpass);
                return true;
            }
        }
        else {
             JOptionPane.showMessageDialog(null, "Mật Khẩu Không Khớp!!!"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
        }
        return false;
    }
//</editor-fold>
    //==========================================================================================================
    //<editor-fold defaultstate="collapsed" desc="Form Nhân Sự - Quyền Admin">
    //Thêm Xóa Sửa Nhân Viên
    public boolean stayInRoom(String ma) throws SQLException{
        connect();
        String query = "select distinct nv.manhanvien,tennhanvien,machucvu\n" +
                        "from nhanvien nv\n" +
                        "where machucvu > 3 and manhanvien = '"+ma+"' and  manhanvien in\n" +
                        "	( select distinct manhanvien \n" +
                        "		from THOIGIANNHANPHONG\n" +
                        "		where ngayroiphong is null\n" +
                        "	) ";
        ResultSet rs = stmt.executeQuery(query);
        if(rs.next()) return true;
        return false;
    }
    public void updateStaff(String ma,String ten,String ngaysinh,String diachi,String sdt,boolean gioitinh,String chucvu,String bonus,DefaultTableModel modeltemp) throws SQLException, NoSuchAlgorithmException{
        Object[] options = {"Yes", "No"}; // hỏi yes no để xác nhận xóa
        int dialogresult = JOptionPane.showOptionDialog(null, "Xác nhân cập nhật nhân viên?"," Warning ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon1, options, options[1]);
        if (dialogresult == 0){
            connect();
            int machucvu = 0;
            int sex = 0;
            if(gioitinh) sex = 1; // set giới tính
            //<editor-fold defaultstate="collapsed" desc="Set Chức Vụ">
            String getidchucvu = "select machucvu from chucvu where tenchucvu = N'"+chucvu+"'"; // lấy mã chức vụ
            ResultSet rs = stmt.executeQuery(getidchucvu);
            while(rs.next()){
                machucvu = rs.getInt(1); // set mã nhân viên gán với chức vụ
            }
            //</editor-fold>
            String query = 
                "Update nhanvien set machucvu = '"+machucvu+"', tienthuong1congviec = "+bonus+","
                    + "tennhanvien = N'"+ten+"' , sdt='"+sdt+"', ngaysinh = '"+ngaysinh+"',"
                    + "diachi=N'"+diachi+"', gioitinh = "+sex+" where manhanvien='"+ma+"'";
            stmt.execute(query);
            addTHOIGIANNHANVIEC(ma, machucvu);
                JOptionPane.showMessageDialog(null, "Cập Nhật Nhân Viên Thành Công"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
            showNhanVien(modeltemp);
        }
    }
    public void addStaff(String ten,String ngaysinh,String diachi,String sdt,boolean gioitinh, String chucvu,String bonus,DefaultTableModel modeltemp) throws SQLException, NoSuchAlgorithmException{
        Object[] options = {"Yes", "No"}; // hỏi yes no để xác nhận xóa
        int dialogresult = JOptionPane.showOptionDialog(null, "Xác Nhận Thêm Nhân Viên?"," Warning ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon1, options, options[1]);
        if (dialogresult == 0){
            connect();
            // tạo tài khoản=========================================================================================
            int machucvu = 0;
            int sex = 0;
            if(gioitinh) sex = 1;
            String getidchucvu = "select machucvu from chucvu where tenchucvu = N'"+chucvu+"'";
            ResultSet rs = stmt.executeQuery(getidchucvu);
            while(rs.next()){
                machucvu = rs.getInt(1);
            }
            String ma = "";
            // kiểm tra tồn tại mã
            do {
                int code = (int) Math.floor(((Math.random() * 899999) + 100000));
                ma = String.valueOf(code); // khởi tạo tài khoản
            } while (checkma(ma));
            //======================================================================================
            String matkhau = MD5(ma); // cho mật khẩu và tài khoản trùng nhau
            String query = "insert into nhanvien values('"+ma+"',N'"+ten+"','"+ngaysinh+"',N'"+diachi+"','"+sdt+"',"+sex+",'"+matkhau+"',1,'"+machucvu+"','"+bonus+"',0)";
            stmt.execute(query);//chạy query
            addTHOIGIANNHANVIEC(ma, machucvu);// add thơi gian nhận việc cho nhân viên
            JOptionPane.showMessageDialog(null, "Thêm Nhân Viên Thành Công \n Tài Khoản : "+ma+"\n Mật Khẩu : "+ma+""," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon3);
            showNhanVien(modeltemp);
            }
    }
    
    private boolean checkPMProjectStatus(String projectManagerID) throws SQLException{ // kiểm tra xem pm này còn dự án không, nếu còn thì trả về true
        connect();
        String query = "select maduan from duan where manhanvien = '"+projectManagerID+"' and trangthaiduan = 0";
        ResultSet rs = stmt.executeQuery(query);
        if(rs.next()) return true;
        return false;
    }
    public void disableStaff(String ma) throws SQLException{
        connect();
        Statement stmt1 = conn.createStatement();
        String querycheck = "select trangthainhanvien from nhanvien where manhanvien = '"+ma+"'";
        ResultSet rs = stmt.executeQuery(querycheck);
        while (rs.next()) {            
            if(checkPMProjectStatus(ma)) 
                JOptionPane.showMessageDialog(null, "Không Thể Xóa\\nProject Manager này vẫn còn dự án"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            else if(!rs.getBoolean("trangthainhanvien"))
             JOptionPane.showMessageDialog(null, "Bạn Đã Xóa Nhân Viên Này!!!"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            else {
                String query = "update nhanvien set trangthainhanvien = 0 where manhanvien = '"+ma+"'";
                stmt1.execute(query);
                JOptionPane.showMessageDialog(null, "Khóa Tài Khoản Nhân Viên Thành Công"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
            }
        }
    }
    
    public void ActiveNhanVien(String ma,DefaultTableModel modeltemp) throws SQLException{
        Object[] options = {"Yes", "No"}; // hỏi yes no để xác nhận kích hoạt
        int dialogresult = JOptionPane.showOptionDialog(null, "Xác Nhận Kích Hoạt Nhân Viên?"," Warning ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon1, options, options[1]);
        if (dialogresult == 0){
            connect();
            String query = "update nhanvien set trangthainhanvien = 1, solandangnhap = 0  where manhanvien = '"+ma+"'";
            stmt.execute(query);
            JOptionPane.showMessageDialog(null, "Kích Hoạt Nhân Viên Thành Công"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
            showNhanVien(modeltemp);
        }
        
    }
    public boolean checkma(String ma) throws SQLException{
        connect();
        String query = "Select* from NhanVien";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            if(rs.getString("manhanvien").equals(ma)) return true; // tồn tại mã nhân viên thì update
        }
        return false; // không tồn tại thì add
    }
    public void resetpassword(String ma) throws NoSuchAlgorithmException, SQLException{
        Object[] options = {"Yes", "No"}; // hỏi yes no để xác nhận kích hoạt
        int dialogresult = JOptionPane.showOptionDialog(null, "Xác Nhận Đặt Lại Mật Khẩu Cho Nhân Viên?"," Warning ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon1, options, options[1]);
        if (dialogresult == 0){
            connect();
            String matkhau = ma;
            String matkhaumd5 = MD5(ma);
            String query = "update nhanvien set matkhau = '"+matkhaumd5+"' where manhanvien = '"+ma+"'";
            stmt.execute(query);
            JOptionPane.showMessageDialog(null, "Reset mật khẩu thành công \n Mật khẩu mới : "+matkhau+""," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }
    }
    
    //============================================================Tính Lương========================================================================================
    private ResultSet getStaff() throws SQLException{ // dùng để tính lương
        connect();
        String query = "select manhanvien,machucvu from NHANVIEN order by machucvu asc";
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }
    private void addPersonnelSalary(String staffID,String startingDate,String endingDate,DefaultTableModel modeltemp) throws SQLException{
        connect();
        String query = "exec tinhluongNS '"+staffID+"' , '"+startingDate+"' , '"+endingDate+"'";
        ResultSet rs = stmt.executeQuery(query);
        addSalary(modeltemp, rs);
    }
    private void addProjectManagerSalary(String staffID,String startingDate,String endingDate,DefaultTableModel modeltemp) throws SQLException{
        connect();
        String query = "exec tinhluongPM '"+staffID+"' , '"+startingDate+"' , '"+endingDate+"'";
        ResultSet rs = stmt.executeQuery(query);
        addSalary(modeltemp, rs);
    }
    private void addLeaderSalary(String staffID,String startingDate,String endingDate,DefaultTableModel modeltemp) throws SQLException{
        connect();
        String query = "exec tinhluongLD '"+staffID+"' , '"+startingDate+"' , '"+endingDate+"'";
        ResultSet rs = stmt.executeQuery(query);
        addSalary(modeltemp, rs);
    }
    private void addStaffSalary(String staffID,String startingDate,String endingDate,DefaultTableModel modeltemp) throws SQLException{
        connect();
        String query = "exec tinhluongNV '"+staffID+"' , '"+startingDate+"' , '"+endingDate+"'";
        ResultSet rs = stmt.executeQuery(query);
        addSalary(modeltemp, rs);
    }
    public void getSalary(DefaultTableModel modeltemp,String startingDate,String endingDate) throws SQLException{ // tính lương
        connect();
        modeltemp.setRowCount(0); // clear jtable
        ResultSet rsStaff = getStaff();
        while (rsStaff.next()){
            String staffUID = rsStaff.getString(1); // mã nhân viên
            switch(rsStaff.getInt(2)){ // mã chức vụ
                case 1:
                    addPersonnelSalary(staffUID, startingDate, endingDate,modeltemp);
                    break;
                case 2:
                    addProjectManagerSalary(staffUID, startingDate, endingDate,modeltemp);
                    break;
                case 3:
                    addLeaderSalary(staffUID, startingDate, endingDate,modeltemp);
                    break;
                case 4:
                    addStaffSalary(staffUID, startingDate, endingDate,modeltemp);
                    break;
            }
        }
    }
    private void addSalary(DefaultTableModel modeltemp,ResultSet rs) throws SQLException{
        while (rs.next()){
            String staffID = rs.getString(1);
            String staffName = rs.getString(2);
            String staffPosition = rs.getString(3);
            String basicSalary = String.valueOf(rs.getInt(4));
            String bonus = String.valueOf(rs.getInt(5));
            String salary = String.valueOf(rs.getInt(6));
            String tbData[] = {staffID,staffName,staffPosition,basicSalary,bonus,salary}; 
            modeltemp.addRow(tbData);
        }
    }
    //====================================================================================================================================================
    private String getQuerySalaryDetails(String staffID,int staffPositionID,String startingDate,String endingDate){ // lấy query lương cụ thể của 1 nhân viên dựa vào 4 procedure
        String query = "";
        switch(staffPositionID){
            case 1: 
                query = "exec tinhluongNS '"+staffID+"' , '"+startingDate+"' , '"+endingDate+"'";
                break;
            case 2:    
                query = "exec tinhluongPM '"+staffID+"' , '"+startingDate+"' , '"+endingDate+"'";
                break;
            case 3:    
                query = "exec tinhluongLD '"+staffID+"' , '"+startingDate+"' , '"+endingDate+"'";
                break;
            case 4:    
                query = "exec tinhluongNV '"+staffID+"' , '"+startingDate+"' , '"+endingDate+"'";
                break;
        }
        return query;
    }
    public String getNumberOfTask(String staffID,String staffPositionName,String startingDate,String endingDate) throws SQLException{ //lấy số lượng công việc của 1 nhân viên
        int staffPositionID = getPositionID(staffPositionName);
        if(staffPositionID == 1) return "0";
        connect();
        String query = getQuerySalaryDetails(staffID, staffPositionID, startingDate, endingDate);
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next())
        {            
            return rs.getString("soluongcv");
        }
        return "";
    }
    public String getBonusForATask(String staffID,String staffPositionName,String startingDate,String endingDate) throws SQLException{ // lấy tiền thưởng 1 công việc của 1 nhân viên
        int staffPositionID = getPositionID(staffPositionName);
        connect();
        String query = getQuerySalaryDetails(staffID, staffPositionID, startingDate, endingDate);
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next())
        {            
            return rs.getString("tienthuong1congviec");
        }
        return "";
    }
    //</editor-fold>
    //==========================================================================================================
    //<editor-fold defaultstate="collapsed" desc="mã hóa MD5">
    public String MD5(String pass) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(pass.getBytes());
        byte[] byteData = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            String hex = Integer.toHexString(0xff & byteData[i]);
           sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
         return sb.toString();
     }
     //</editor-fold>
    //==========================================================================================================
    //<editor-fold defaultstate="collapsed" desc="Hàm Dùng Lại Nhiều Lần">
    public ResultSet getTHOIGIANNHANVIEC(String ma) throws SQLException{
        connect();
        String querychucvu = "select top 1 * from THOIGIANNHANVIEC where manhanvien = '"+ma+"' order by ngaynhancv desc";
        ResultSet rs = stmt.executeQuery(querychucvu);
        return rs;
    }
    public void addTHOIGIANNHANVIEC(String ma, int machucvu) throws SQLException{
        connect();
        Statement stmt1 = conn.createStatement();
        String checkmakhongdoi = "select top 1 * from THOIGIANNHANVIEC where manhanvien = '"+ma+"' order by ngaynhancv desc";
        ResultSet rs = stmt1.executeQuery(checkmakhongdoi); // nếu không đổi chức vụ thì không add thời gian nhận việc mới
        if (!rs.next()) {            
            String query1 = "insert into THOIGIANNHANVIEC values('"+ma+"',"+machucvu+",current_timestamp)";
            stmt.execute(query1);
        }else if(!(rs.getInt("machucvu") == machucvu)){
            Statement stmt2 = conn.createStatement();
            String query1 = "insert into THOIGIANNHANVIEC values('"+ma+"',"+machucvu+",current_timestamp)";
            stmt2.execute(query1);
        }
    }
    public int CheckChucVu_TruyenMaNhanvien(String manhanvien) throws SQLException{
        connect();
        String query = "select machucvu from nhanvien where manhanvien = '"+manhanvien+"'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {            
            return rs.getInt(1);
        }
        return 0;
    }
    public int KiemTraKieuDuLieuCuaChuoi(String chuoi){
        Character c = chuoi.charAt(0);
        int flag;
        if(Character.isDigit(c)) flag = 1; 
        else flag = 2;
        int kytuhientai = 0;
        for (int i = 0; i < chuoi.length(); i++) {
            if(Character.isDigit(chuoi.charAt(i))) kytuhientai = 1;
            else kytuhientai = 2;
            if(flag != kytuhientai) return 3;
        }
        return kytuhientai;
    }
    public String getRoomID(String tenphong) throws SQLException{
            connect();
            String query = "select maphong from phong where tenphong = N'"+tenphong+"'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {            
                return rs.getString(1);
            }
            return null;
        }
    public String getMaleader(String tenphong) throws SQLException{
            connect();
            String maphong = getRoomID(tenphong);
            String query = "select manhanvien from phong where maphong = '"+maphong+"'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {                
                return  rs.getString(1);
            }
            return "";
        }
    public int getPositionID(String positionName) throws SQLException{
        connect();
        String query = "select machucvu from chucvu where tenchucvu =N'"+positionName+"'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next())
        {            
            return rs.getInt(1);
        }
        return 4;
    }
    public String getRoomName(String roomID) throws SQLException{
        connect();
        String query = "select tenphong from phong where maphong = '"+roomID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next())
        {            
            return rs.getString(1);
        }
        return "Không Tồn Tại";
    }
    public String getPositionName(int positionID) throws SQLException{
        connect();
        String query = "select tenchucvu from CHUCVU where machucvu = "+positionID+"";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()){
            return rs.getString(1);
        }
        return "";
    }
    public String getStaffName(String staffID) throws SQLException{
        connect();
        String query = "select tennhanvien from nhanvien where manhanvien ='"+staffID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()){
            return rs.getString(1);
        }
        return "";
    }   
    public String getProjectID(String projectName) throws SQLException{
        connect();
        String query = "select maduan from duan where tenduan = N'"+projectName+"'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()){
            return rs.getString(1);
        }
        return "";
    }
    public String getProjectStartingDate(String projectID) throws SQLException{
        connect();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        String query = "select thoigianbatdauduan from duan where maduan = '"+projectID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            return  date.format(rs.getDate(1));
        }
        return "";
    }
    public boolean checkDateBetweenTaskAndProject(String startingDateTask, String projectID) throws SQLException, ParseException{ // nếu ngày bắt đầu dự án nhỏ hơn ngày bắt đầu công việc thì trả về true
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        if(date.parse(getProjectStartingDate(projectID)).after(date.parse(startingDateTask))) return true;
        return false;
    }
    public ArrayList<String> getStaff(String staffID) throws SQLException{
        connect();
        ArrayList<String> obj = new ArrayList<>();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        String query = "select * from nhanvien where manhanvien = '"+staffID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            obj.add(rs.getString(1));
            obj.add(rs.getString(2));
            obj.add(date.format(rs.getDate(3)));
            obj.add(rs.getString(4));
            obj.add(rs.getString(5));
            obj.add(rs.getString(6));
            
            obj.add(rs.getString(8));
            obj.add(rs.getString(9));
            obj.add(rs.getString(10));
        }
        return obj;
    }
    //</editor-fold>
    //==========================================================================================================
    //<editor-fold defaultstate="collapsed" desc="Form Phòng - Quyền Admin">
    //<editor-fold defaultstate="collapsed" desc="Form Quản Lý Phòng">
        public void showcomboboxphong(DefaultComboBoxModel aBoxModel) throws SQLException{
            connect();
            String query = "Select * from phong";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {                
                aBoxModel.addElement(rs.getString("tenphong"));
            }
        }
        private boolean checkRoomName(String roomName) throws SQLException{ // true nếu tồn tại tên phòng
            connect();
            String query = "select maphong from phong where tenphong = N'"+roomName+"'";
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next()) return true;
            return false;
        }
        public void CreateRoom(String maphong, String tenphong, String maLeader) throws SQLException{
            if(checkRoomName(tenphong)) JOptionPane.showMessageDialog(null, "Tên Phòng Không Được Trùng");
            else if(!checkroom(maphong)){
               connect();
               String query = "insert into PHONG values('"+maphong+"',N'"+tenphong+"','"+maLeader+"')";
               stmt.execute(query);
               nhanphong(maLeader, maphong);
               JOptionPane.showMessageDialog(null, "Add Thành Công"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
            }else 
               JOptionPane.showMessageDialog(null, "Mã Phòng Đã Tồn Tại"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);            
        }
        public void DeleteNhanVienRaKhoiPhong_FormPhong(String manhanvien,String tenphong) throws SQLException{
            connect();
            String maphong = getRoomID(tenphong);
            String query = "update THOIGIANNHANPHONG\n" +
                            "set ngayroiphong = CURRENT_TIMESTAMP\n" +
                            "where manhanvien = N'"+manhanvien+"' and maphong = '"+maphong+"'";
            stmt.execute(query);
        }

            //<editor-fold defaultstate="collapsed" desc="Add nhân viên vào THOIGIANNHANPHONG">
            public void nhanphong(String manhanvien,String maphong) throws SQLException{
                connect();
                if(!DaNamTrongPhong(manhanvien,maphong) && checkStaffActive(manhanvien)){ // kiểm tra nhân viên đã có phòng chưa
                    String query = "insert into THOIGIANNHANPHONG values('"+manhanvien+"','"+maphong+"',current_timestamp,null)";
                    stmt.execute(query);
                    System.out.println("Insert manhanvien : " + manhanvien);
                }
                else System.out.println("Mã nhân viên : " +manhanvien+ " đã có phòng");
            }
            private boolean checkStaffActive(String staffID) throws SQLException{
                connect();
                String query = "select trangthainhanvien from nhanvien where manhanvien = '"+staffID+"' ";
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()){
                    if(rs.getBoolean(1)) return true;
                }
                return false;
            }
            public boolean DaNamTrongPhong(String manhanvien, String maphong) throws SQLException{
                connect();
                String query = "select top 1 * from THOIGIANNHANPHONG\n" +
                                "where manhanvien = '"+manhanvien+"' and maphong = '"+maphong+"'" +
                                 "order by ngaynhanphong desc";
                ResultSet rs = stmt.executeQuery(query);
                Statement stmt1 = conn.createStatement();
                ResultSet rs1 = stmt1.executeQuery(query);
                if(!rs.next()) return false;
                while(rs1.next()) {                
                    if(rs1.getDate("ngayroiphong") != null) return false;
                }
                return true;
            }
            public void AddNhanVienVaoPhong(ArrayList<String> chuoimanhanvien, String tenphong) throws SQLException{
                String maphong = getRoomID(tenphong); // lấy mã phòng từ tên phòng
                for (String obj : chuoimanhanvien){
                    nhanphong(obj, maphong);
                }
            }
            
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Xuất nhân viên vào 2 jtable">
            public void showStaffOutRoom(DefaultTableModel model, String tenphong) throws SQLException{
                connect();
                String maphong = getRoomID(tenphong);
                String query = "select distinct nv.manhanvien,tennhanvien,machucvu\n" +
                                "from nhanvien nv\n" +
                                "where machucvu > 3 and trangthainhanvien = 1 and manhanvien not in \n" +
                                "	( select distinct manhanvien \n" +
                                "		from THOIGIANNHANPHONG\n" +
                                "		where maphong = '"+maphong+"' and ngayroiphong is null\n" +
                                "	) ";
                ResultSet rs = stmt.executeQuery(query);
                model.setRowCount(0); // set số dòng bằng 0
                while(rs.next()){
                    String manhanvien = rs.getString(1);
                    String tennhanvien = rs.getString(2);

                    String dataString[] = {manhanvien,tennhanvien};
                    model.addRow(dataString);
                }
            }
            public void showStaffInRoom(DefaultTableModel model, String tenphong) throws SQLException{
                connect();
                String maphong = getRoomID(tenphong);
                String query = "select distinct nv.manhanvien,tennhanvien,machucvu\n" +
                                "from nhanvien nv\n" +
                                "where machucvu > 2 and trangthainhanvien = 1 and manhanvien in \n" +
                                "	( select distinct manhanvien \n" +
                                "		from THOIGIANNHANPHONG\n" +
                                "		where maphong = '"+maphong+"' and ngayroiphong is null\n" +
                                "	) ";
                ResultSet rs = stmt.executeQuery(query);
                model.setRowCount(0); // set số dòng bằng 0
                while(rs.next()){
                    String manhanvien = rs.getString(1);
                    String tennhanvien = rs.getString(2);

                    String dataString[] = {manhanvien,tennhanvien};
                    model.addRow(dataString);
                }
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Tìm Kiếm Nhân Viên">
            public void SearchNhanVienTheoMa_CoMaPhong(DefaultTableModel modeltemp,String manhanvien,String tenphong) throws SQLException{
                connect();
                String maphong = getRoomID(tenphong);
                String query = "exec TimNhanVienTheoMa_CoMaPhong '"+manhanvien+"','"+maphong+"'";
                ResultSet rs = stmt.executeQuery(query);
                modeltemp.setRowCount(0); // set số dòng bằng 0
                while(rs.next()){
                    String ma = rs.getString("manhanvien");
                    String ten = rs.getString("tennhanvien");

                    String dataString[] = {ma,ten};
                    modeltemp.addRow(dataString);
                }
            }
            public void SearchNhanVienTheoTen_CoMaPhong(DefaultTableModel modeltemp,String tennhanvien,String tenphong) throws SQLException{
                connect();
                String maphong = getRoomID(tenphong);
                String query = "exec TimNhanVienTheoTen_CoMaPhong N'"+tennhanvien+"','"+maphong+"'";
                ResultSet rs = stmt.executeQuery(query);
                modeltemp.setRowCount(0); // set số dòng bằng 0
                while(rs.next()){
                    String ma = rs.getString("manhanvien");
                    String ten = rs.getString("tennhanvien");

                    String dataString[] = {ma,ten};
                    modeltemp.addRow(dataString);
                }
            }
            //</editor-fold>
        public boolean checkroom(String maphong) throws SQLException{
            connect();
            String query = "select * from phong where maphong = '"+maphong+"'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {            
                return true;
            }
            return false;
        }
        public boolean checkLeader(String maLeader) throws SQLException{
            connect();
            String query = "select * from phong where manhanvien = '"+maLeader+"'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {            
                return true;
            }
            return false;
        } 
        public String TenLeader(String tenphong) throws SQLException{
            connect();
            String query = "select tennhanvien from NHANVIEN nv, PHONG p\n" +
                            "where nv.manhanvien=p.manhanvien and tenphong = N'"+tenphong+"'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {            
                return rs.getString(1);
            }
            return "Không Tồn Tại";
        }
        public void showLeader(DefaultTableModel model) throws SQLException{
            connect();
            String query = "select * from nhanvien where machucvu = 3 and trangthainhanvien = 1";
            model.setRowCount(0);
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {            
                String maLeader = rs.getString("manhanvien");
                String tenLeader = rs.getString("tennhanvien");
                String tbData[] = {maLeader,tenLeader}; 
                model.addRow(tbData);
            }
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Form Đổi Leader Phòng">
        public void ShowLeader_FormChangeLeader(DefaultTableModel model, String tenphong) throws SQLException{
            connect();
            String maphong = getRoomID(tenphong);
            String query = "select distinct nv.manhanvien,tennhanvien,machucvu\n" +
                            "from nhanvien nv\n" +
                            "where machucvu = 3 and trangthainhanvien = 1 and manhanvien not in \n" +
                            "	( select distinct manhanvien \n" +
                            "		from THOIGIANNHANPHONG\n" +
                            "		where maphong = '"+maphong+"' and ngayroiphong is null\n" +
                            "	) ";
            ResultSet rs = stmt.executeQuery(query);
            model.setRowCount(0); // set số dòng bằng 0
            while(rs.next()){
                String manhanvien = rs.getString(1);
                String tennhanvien = rs.getString(2);

                String dataString[] = {manhanvien,tennhanvien};
                model.addRow(dataString);
            }
            
        }
        public void ChangeRoomLeader_FormChangeLeader(String manhanvien, String tenphong,DefaultTableModel model) throws SQLException{
            Object[] options = {"Yes", "No"}; // hỏi yes no để xác nhận kích hoạt
            int dialogresult = JOptionPane.showOptionDialog(null, "Xác Nhận Đổi Leader?"," Warning ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon1, options, options[1]);
            if (dialogresult == 0){
                connect();
                String maphong = getRoomID(tenphong); // lấy mã phòng
                String OldLeaderID = getMaleader(tenphong); // lẫy mã leader cũ
                DeleteNhanVienRaKhoiPhong_FormPhong(OldLeaderID, tenphong); // delete Leader cũ (thêm ngày rời phòng)
                LeaderNhanPhongMoi(manhanvien,maphong); // add leader mới (add vào table phòng)
                ArrayList<String> chuoi = new ArrayList<String>(); // tạo object để add leader vào table thời gian nhận phòng
                chuoi.add(manhanvien);
                AddNhanVienVaoPhong(chuoi,tenphong); // add leader vào thời gian nhận phòng 
                JOptionPane.showMessageDialog(null, "Đổi Leader Thành Công","Thông báo",JOptionPane.INFORMATION_MESSAGE,icon1);
                ShowLeader_FormChangeLeader(model, tenphong);
            }
        }
        public void LeaderNhanPhongMoi(String manhanvien, String maphong) throws SQLException{
        connect();
        String query = "update phong set manhanvien = '"+manhanvien+"' where maphong = '"+maphong+"'";
        stmt.execute(query);
        }
        
    //</editor-fold>
    //</editor-fold>
    //==========================================================================================================
    //<editor-fold defaultstate="collapsed" desc="Dự Án">
    //<editor-fold defaultstate="collapsed" desc="Tạo Dự Án">
        private boolean checkProjectName(String projectName) throws SQLException{ // true nếu tồn tại tên dự án
            connect();
            String query = "select maduan from duan where tenduan = N'"+projectName+"'";
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next()) return true;
            return false;
        }
        public int getCountProject() throws SQLException{
            connect();
            String query = "select count(*) from duan";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {                
                return rs.getInt(1);
            }
            return 0;
        }
        public void createProject(String projectName,String projectStartingDate,String projectEndingDate,String roomName,String descriptionProject) throws SQLException{
            if(!checkProjectName(projectName)){
                String projectID = String.valueOf(getCountProject() + 1); // đếm số lượng dự án và tăng lên 1
                String userLoginID = NhanVien.getInstance().getManhanvien(); 
                String roomID = getRoomID(roomName);
                connect();
                String query = "insert into DUAN values('"+projectID+"',N'"+projectName+"','"+userLoginID+"',N'"+descriptionProject+"','"+projectStartingDate+"','"+projectEndingDate+"',0,'"+roomID+"')";
                stmt.execute(query);
                JOptionPane.showMessageDialog(null, "Tạo Dự Án Thành Công!!!"," Thông báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
            }else 
//                JOptionPane.showMessageDialog(null, "Tên Dự Án Không Được Trùng");
            JOptionPane.showMessageDialog(null, "Tên Dự Án Không Được Trùng"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Show Nhân Viên lên Jtabel">
    public void showProject(DefaultTableModel modeltemp) throws SQLException{
        if(NhanVien.getInstance().getMachucvu() == 1) showProjectAdmin(modeltemp);
        else if(NhanVien.getInstance().getMachucvu() == 2) showProjectManager(modeltemp);
        else if(NhanVien.getInstance().getMachucvu() == 3) showProjectLeader(modeltemp);
    }
    private void showProjectManager(DefaultTableModel modeltemp) throws SQLException{ // show danh sách dự án của manager 
        connect();
        modeltemp.setRowCount(0); // clear jtable
        String query = "select * from duan where manhanvien = '"+NhanVien.getInstance().getManhanvien()+"' order by trangthaiduan asc";
        ResultSet rs = stmt.executeQuery(query);
        String projectStatus = "Chưa Hoàn Thành";
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        String endingProjectDate = "";
        while (rs.next())
        {            
            String projectID = rs.getString("maduan");
            String projectName = rs.getString("tenduan");
            String projectManagerID = rs.getString("manhanvien");
            String projectDescription = rs.getString("noidungduan");
            String startingProjectDate = date.format(rs.getDate("thoigianbatdauduan"));
            if (!StringUtils.isEmpty(rs.getString("thoigianketthucduan"))) endingProjectDate = date.format(rs.getDate("thoigianketthucduan"));
            if(rs.getBoolean("trangthaiduan")) projectStatus = "Hoàn Thành";
            String roomName = getRoomName(rs.getString("maphong"));
            String tbData[] = {projectID,projectName,startingProjectDate,endingProjectDate,projectStatus,roomName}; 
            modeltemp.addRow(tbData);
        }
    }
    private void showProjectAdmin(DefaultTableModel modeltemp) throws SQLException{ // show danh sách dự án của manager 
        connect();
        modeltemp.setRowCount(0); // clear jtable
        String query = "select * from duan order by trangthaiduan asc";
        ResultSet rs = stmt.executeQuery(query);
        String projectStatus = "Chưa Hoàn Thành";
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        String endingProjectDate = "";
        while (rs.next())
        {            
            String projectID = rs.getString("maduan");
            String projectName = rs.getString("tenduan");
            String projectManagerID = rs.getString("manhanvien");
            String projectDescription = rs.getString("noidungduan");
            String startingProjectDate = date.format(rs.getDate("thoigianbatdauduan"));
            if (!StringUtils.isEmpty(rs.getString("thoigianketthucduan"))) endingProjectDate = date.format(rs.getDate("thoigianketthucduan"));
            if(rs.getBoolean("trangthaiduan")) projectStatus = "Hoàn Thành";
            String roomName = getRoomName(rs.getString("maphong"));  // lấy tên phòng
            
            String projectManagerName = getStaffName(projectManagerID); // lấy tên project manager qua mã nhân viên
            String tbData[] = {projectID,projectName,projectManagerName,startingProjectDate,endingProjectDate,projectStatus,roomName}; 
            modeltemp.addRow(tbData);
        }
    }
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="Tìm Kiếm Dự Án">
    private void searchProjectIDWithAdmin(DefaultTableModel modeltemp,String chuoi) throws SQLException{
        connect();
        modeltemp.setRowCount(0);
        String query = "select * from duan where maduan = "+chuoi+" order by trangthaiduan asc";
        ResultSet rs = stmt.executeQuery(query);
        String projectStatus = "Chưa Hoàn Thành";
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        String endingProjectDate = "";
        while (rs.next())
        {            
            String projectID = rs.getString("maduan");
            String projectName = rs.getString("tenduan");
            String projectManagerID = rs.getString("manhanvien");
            String projectDescription = rs.getString("noidungduan");
            String startingProjectDate = date.format(rs.getDate("thoigianbatdauduan"));
            if (!StringUtils.isEmpty(rs.getString("thoigianketthucduan"))) endingProjectDate = date.format(rs.getDate("thoigianketthucduan"));
            if(rs.getBoolean("trangthaiduan")) projectStatus = "Hoàn Thành";
            String roomName = getRoomName(rs.getString("maphong"));
            
            String projectManagerName = getStaffName(projectManagerID); // lấy tên project manager qua mã nhân viên
            String tbData[] = {projectID,projectName,projectManagerName,startingProjectDate,endingProjectDate,projectStatus,roomName}; 
            modeltemp.addRow(tbData);
        }
    }
    private void searchProjectNameWithAdmin(DefaultTableModel modeltemp,String chuoi) throws SQLException{
        connect();
        modeltemp.setRowCount(0);
        String query = "select * from duan where dbo.ufn_removeMark(tenduan) like N'%"+chuoi+"%' or tenduan like N'%"+chuoi+"%' order by trangthaiduan asc";
        ResultSet rs = stmt.executeQuery(query);
        String projectStatus = "Chưa Hoàn Thành";
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        String endingProjectDate = "";
        while (rs.next())
        {            
            String projectID = rs.getString("maduan");
            String projectName = rs.getString("tenduan");
            String projectManagerID = rs.getString("manhanvien");
            String projectDescription = rs.getString("noidungduan");
            String startingProjectDate = date.format(rs.getDate("thoigianbatdauduan"));
            if (!StringUtils.isEmpty(rs.getString("thoigianketthucduan"))) endingProjectDate = date.format(rs.getDate("thoigianketthucduan"));
            if(rs.getBoolean("trangthaiduan")) projectStatus = "Hoàn Thành";
            String roomName = getRoomName(rs.getString("maphong"));
            
            String projectManagerName = getStaffName(projectManagerID); // lấy tên project manager qua mã nhân viên
            String tbData[] = {projectID,projectName,projectManagerName,startingProjectDate,endingProjectDate,projectStatus,roomName}; 
            modeltemp.addRow(tbData);
        }
    }
    
    private void searchProjectIDWithPM(DefaultTableModel modeltemp,String chuoi) throws SQLException{
        connect();
        modeltemp.setRowCount(0);
        String query = "select * from duan where maduan = "+chuoi+" and manhanvien = "+NhanVien.getInstance().getManhanvien()+" order by trangthaiduan asc";
        ResultSet rs = stmt.executeQuery(query);
        String projectStatus = "Chưa Hoàn Thành";
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        String endingProjectDate = "";
        while (rs.next())
        {            
            String projectID = rs.getString("maduan");
            String projectName = rs.getString("tenduan");
            String projectManagerID = rs.getString("manhanvien");
            String projectDescription = rs.getString("noidungduan");
            String startingProjectDate = date.format(rs.getDate("thoigianbatdauduan"));
            if (!StringUtils.isEmpty(rs.getString("thoigianketthucduan"))) endingProjectDate = date.format(rs.getDate("thoigianketthucduan"));
            if(rs.getBoolean("trangthaiduan")) projectStatus = "Hoàn Thành";
            String roomName = getRoomName(rs.getString("maphong"));
            String tbData[] = {projectID,projectName,startingProjectDate,endingProjectDate,projectStatus,roomName}; 
            modeltemp.addRow(tbData);
        }
    }
    private void searchProjectNameWithPM(DefaultTableModel modeltemp,String chuoi) throws SQLException{
        connect();
        modeltemp.setRowCount(0);
        String query = "select * from duan where dbo.ufn_removeMark(tenduan) like N'%"+chuoi+"%' or tenduan like N'%"+chuoi+"%' and manhanvien = '"+NhanVien.getInstance().getManhanvien()+"' order by trangthaiduan asc";
        ResultSet rs = stmt.executeQuery(query);
        String projectStatus = "Chưa Hoàn Thành";
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        String endingProjectDate = "";
        while (rs.next())
        {            
            String projectID = rs.getString("maduan");
            String projectName = rs.getString("tenduan");
            String projectManagerID = rs.getString("manhanvien");
            String projectDescription = rs.getString("noidungduan");
            String startingProjectDate = date.format(rs.getDate("thoigianbatdauduan"));
            if (!StringUtils.isEmpty(rs.getString("thoigianketthucduan"))) endingProjectDate = date.format(rs.getDate("thoigianketthucduan"));
            if(rs.getBoolean("trangthaiduan")) projectStatus = "Hoàn Thành";
            String roomName = getRoomName(rs.getString("maphong"));
            String tbData[] = {projectID,projectName,startingProjectDate,endingProjectDate,projectStatus,roomName}; 
            modeltemp.addRow(tbData);
        }
    }
    
    public void searchProjectWithProjectID(DefaultTableModel modeltemp,String chuoi) throws SQLException{
        if(NhanVien.getInstance().getMachucvu() == 1) searchProjectIDWithAdmin(modeltemp, chuoi);
        else if(NhanVien.getInstance().getMachucvu() == 2) searchProjectIDWithPM(modeltemp, chuoi);
    }
    public void searchProjectWithProjectName(DefaultTableModel modeltemp,String chuoi) throws SQLException{
        if(NhanVien.getInstance().getMachucvu() == 1) searchProjectNameWithAdmin(modeltemp, chuoi);
        else if(NhanVien.getInstance().getMachucvu() == 2) searchProjectNameWithPM(modeltemp, chuoi);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Save Project">
    private boolean checkOnDifferentDates(String projectID,String startingProjectDate,String endingProjectDate) throws SQLException{ // trạng thái ngày có sự thay đổi => return true
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        connect();
        String query = "select thoigianbatdauduan,thoigianketthucduan from duan where maduan = '"+projectID+"'";
        ResultSet rs;
        rs = stmt.executeQuery(query);
         while (rs.next()){
             if(!(date.format(rs.getDate(1)).equals(startingProjectDate) && date.format(rs.getDate(2)).equals(endingProjectDate))) 
                 return true;
        }
        return false;
    }
    private void updateProject(String projectID,String startingProjectDate,String endingProjectDate) throws SQLException{
        if(checkOnDifferentDates(projectID, startingProjectDate, endingProjectDate)){
            connect();
            String query = "update duan \n" +
                            "set thoigianbatdauduan = '"+startingProjectDate+"', thoigianketthucduan = '"+endingProjectDate+"', trangthaiduan = 0\n" +
                            "where maduan = '"+projectID+"'";
            stmt.execute(query);
              JOptionPane.showMessageDialog(null, "Cập Nhật Dự Án Thành Công!!!"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }else System.out.println("ngày không có đổi");
    }
    public void saveProject(String projectID,String startingProjectDate,String endingProjectDate) throws SQLException{
        if (checkProjectStatus(projectID)){
            if(NhanVien.getInstance().getMachucvu() == 2) updateProject(projectID, startingProjectDate, endingProjectDate);
            else 
                JOptionPane.showMessageDialog(null, "Không Thể Thay Đổi Dự Án Đã Hoàn Thành"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2); // Nhân Sự không được phép lưu khi dự án đã hoàn thành
        }else updateProject(projectID, startingProjectDate, endingProjectDate);
    }
//</editor-fold>
    private boolean checkProjectStatus(String projectID) throws SQLException{ //dự án hoàn thành => return true
        connect();
        String query = "select trangthaiduan from duan where maduan = '"+projectID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()){
            return rs.getBoolean(1);
        }
        return false;
    }
    private boolean checkTaskStatus(String projectID) throws SQLException{ // nếu còn công việc chưa hoàn thành thì trả về true
        connect();
        String query = "select * from CONGVIEC\n" +
                        "where maduan = '"+projectID+"' and trangthaicongviec is null";
        ResultSet rs = stmt.executeQuery(query);
        if(rs.next()) return true;
        return false;
    }
    private boolean checkCountTask(String projectID) throws SQLException{ // nếu số công việc bằng 0 thì trả về true
        connect();
        String query = "select count(*) from CONGVIEC where maduan = '"+projectID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next())
        {            
            if(rs.getInt(1) == 0) return true;
        }
        return false;
    }
    public void finishProject(String projectID, DefaultTableModel model) throws SQLException{
        if(checkCountTask(projectID)) JOptionPane.showMessageDialog(null, "Không Thể Hoàn Thành Dự Án\nDự Án Này Vẫn Chưa Có Công Việc"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
        else if(checkTaskStatus(projectID)) JOptionPane.showMessageDialog(null, "Không Thể Hoàn Thành Dự Án\nVẫn Còn Công Việc Chưa Hoàn Thành"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
        else if(checkProjectStatus(projectID))
         JOptionPane.showMessageDialog(null, "Dự Án Này Đã Hoàn Thành"," Error ",JOptionPane.INFORMATION_MESSAGE,icon1);
        else {
            Object[] options = {"Yes", "No"}; // hỏi yes no để xác nhận xóa
            int dialogresult = JOptionPane.showOptionDialog(null, "Xác Nhận Hoàn Thành Dự Án?"," Warning ",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon1, options, options[1]);
            if (dialogresult == 0){
                connect();
                String query = "update duan set trangthaiduan = 1 where maduan = '"+projectID+"'";
                stmt.execute(query);
                JOptionPane.showMessageDialog(null, "Cập Nhật Dự Án Thành Công"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon2);
                showProject(model);
            }
        }
    }
    public String getProjectDescription(String projectID) throws SQLException{
        connect();
        String query = "select noidungduan from duan where maduan  = '"+projectID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            return rs.getString(1);
        }
        return "";
    }
    public void showStaffInProject(DefaultTableModel modeltemp,String roomName) throws SQLException{
        connect();
        String roomID = getRoomID(roomName);
        modeltemp.setRowCount(0); // clear jtable
        String query = "select distinct nv.manhanvien,tennhanvien,machucvu\n" +
                        "from nhanvien nv\n" +
                        "where machucvu > 2 and trangthainhanvien = 1 and manhanvien in \n" +
                        "	( select distinct manhanvien \n" +
                        "		from THOIGIANNHANPHONG\n" +
                        "		where maphong = '"+roomID+"' and ngayroiphong is null\n" +
                        "	) order by machucvu asc ";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()){
            String staffID = rs.getString(1);
            String staffName = rs.getString(2);
            int staffPositionID = rs.getInt(3);
            String staffPositionName = getPositionName(staffPositionID);
            String tbData[] = {staffID,staffName,staffPositionName}; 
            modeltemp.addRow(tbData);
        }
    }
    
    //</editor-fold>
    //==========================================================================================================
    //<editor-fold defaultstate="collapsed" desc="Công Việc">
    private String getListProject(){ // query lấy danh sách dự án 
        String a = "";
        if(NhanVien.getInstance().getMachucvu() == 3) // chỉ show dự án của leader đó
            a = "select d.* from duan d inner join phong p \n" +
            "on d.maphong = p.maphong \n" +
            "where trangthaiduan = 0 and d.maphong in (select maphong from phong where manhanvien = '"+NhanVien.getInstance().getManhanvien()+"')";
        else if (NhanVien.getInstance().getMachucvu() == 1 || NhanVien.getInstance().getMachucvu() == 2 ) // show hết nếu là nhân sự hay PM
            a = "select d.* from duan d inner join phong p \n" +
                "on d.maphong = p.maphong \n" +
                "where trangthaiduan = 0 and d.maphong in (select maphong from phong)";
        else if(NhanVien.getInstance().getMachucvu() == 4)
            a = "select distinct tenduan from CONGVIEC cv inner join DUAN da on  cv.maduan = da.maduan\n" +
                "where trangthaiduan = 0 and cv.manhanvien = '"+NhanVien.getInstance().getManhanvien()+"'";
        return a;
    }
    //<editor-fold defaultstate="collapsed" desc="Tạo Công Việc">
    private void showProjectLeader(DefaultTableModel modeltemp) throws SQLException{ // show danh sách dự án của manager 
        connect();
        modeltemp.setRowCount(0);
        String query = getListProject(); // lấy query danh sách dự án của leader
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()){
            String projectID = rs.getString("maduan");
            String projectName = rs.getString("tenduan");
            String tbData[] = {projectID,projectName}; 
            modeltemp.addRow(tbData);
        }
    }
    public void createTask(String projectID, String task) throws SQLException{
        if(!checkEmptyTask(task)){
            connect();
            int numberOfTasks = countProjectTask(projectID);
            String query = "insert into congviec(sttcv,maduan,noidungcongviec,trangthaibaocao) values('"+(numberOfTasks+1)+"','"+projectID+"',N'"+task+"',0)" ;
            stmt.execute(query);
            JOptionPane.showMessageDialog(null, "Tạo Công Việc Thành Công"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }else 
            JOptionPane.showMessageDialog(null, "Vui Lòng Điền Nội Dung Công Việc!!!"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);      
    }
    private int countProjectTask(String projectID) throws SQLException{
        connect();
        String query = "select count(*) from CONGVIEC where maduan = '"+projectID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()){
            return rs.getInt(1);
        }
        return 0;
    }
    private boolean checkEmptyTask(String task){
        return task.equals("");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Update Task">
    private void finishTask(String projectName,String taskID) throws SQLException{
            Object[] options = {"Yes", "No"};
            int dialogresult = JOptionPane.showOptionDialog(null, "Xác Nhận Hoàn Thành Công Việc Được Chọn","Warning",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon4, options, options[1]);
            if (dialogresult == 0){
            connect();
            String projectID = getProjectID(projectName);
            String query = "update congviec set trangthaicongviec = 1 where sttcv = '"+taskID+"' and maduan = '"+projectID+"'";
            stmt.execute(query);
            JOptionPane.showMessageDialog(null, "Cập Nhật Trạng Thái Hoàn Thành Công Việc Được Chọn!!!"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }
    }
    private void deleteTask(String projectName,String taskID) throws SQLException{
            Object[] options = {"Yes", "No"};
            int dialogresult = JOptionPane.showOptionDialog(null, "Xác Nhận Xóa Công Việc Được Chọn","Warning",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon4, options, options[1]);
            if (dialogresult == 0){
            connect();
            String projectID = getProjectID(projectName);
            String query = "update congviec set trangthaicongviec = 0 where sttcv = '"+taskID+"' and maduan = '"+projectID+"'";
            stmt.execute(query);
            JOptionPane.showMessageDialog(null, "Đã Xóa Công Việc Được Chọn!!!"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }
    }
    private void cancelTask(String projectName,String taskID) throws SQLException{
            Object[] options = {"Yes", "No"};
            int dialogresult = JOptionPane.showOptionDialog(null, "Xác Nhận Hủy Bỏ Công Việc Được Chọn","Warning",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, icon4, options, options[1]);
            if (dialogresult == 0){
            connect();
            String projectID = getProjectID(projectName);
            String query = "update congviec set trangthaibaocao = 0,noidungbaocao = null where sttcv = '"+taskID+"' and maduan = '"+projectID+"'";
            stmt.execute(query);
            JOptionPane.showMessageDialog(null, "Đã Hủy Công Việc Được Chọn!!!"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }
    }
    public void updateTask(String projectName,String taskID,int command) throws SQLException{
        if(command == 1) finishTask(projectName, taskID);
        else if(command == 0) deleteTask(projectName, taskID);
        else if(command == 2) cancelTask(projectName, taskID);
    }
    //</editor-fold>
    //Leader =========================================================================================
    private void showComboBoxProjectFormTaskList(DefaultComboBoxModel comboBoxModel) throws SQLException{ // show project của pm quản lý khi nhấn vào form tasklist => check quyền
        connect();
        String query = "select * from DUAN\n" +
                        "where manhanvien = '"+NhanVien.getInstance().getManhanvien()+"' and trangthaiduan = 0";
        ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {                
                comboBoxModel.addElement(rs.getString("tenduan"));
            }
    }
    
    public void showComboboxProject(DefaultComboBoxModel comboBoxModel) throws SQLException{
        if(NhanVien.getInstance().getMachucvu() == 2) showComboBoxProjectFormTaskList(comboBoxModel);
        else {
        connect();
        String query = getListProject();
        ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {                
                comboBoxModel.addElement(rs.getString("tenduan"));
            }
        }
    }
    public void showTaskInFormLeader(DefaultTableModel modeltemp,String projectName) throws SQLException{
        connect();
        modeltemp.setRowCount(0); // clear jtable
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        String projectID = getProjectID(projectName);
        String query = "select * From CONGVIEC\n" +
                        "where maduan = '"+projectID+"'" +
                        "        order by CASE\n" +
                        "	 WHEN  trangthaicongviec is null then 1\n" +
                        "	 WHEN trangthaicongviec = 1  THEN 2\n" +
                        "	 WHEN trangthaicongviec = 0  THEN 3\n" +
                        "        END;";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            String staffname = "";
            String taskStartingDay = "";
            String taskEndingDay = "";
            String taskStatus = "";
            String taskReport = "Chưa Có Báo Cáo";
            
            String taskID = rs.getString("sttcv");
            if (!StringUtils.isEmpty(rs.getString("manhanvien"))) staffname = getStaffName(rs.getString("manhanvien")); // check null
            if (!StringUtils.isEmpty(rs.getString("thoigianbatdaucv"))) taskStartingDay = date.format(rs.getDate("thoigianbatdaucv"));// check null
            if (!StringUtils.isEmpty(rs.getString("thoigianketthuccv"))) taskEndingDay = date.format(rs.getDate("thoigianketthuccv"));// check null
            if (StringUtils.isEmpty(rs.getString("trangthaicongviec"))) taskStatus = "Đang Làm";// check null
            else if(rs.getBoolean("trangthaicongviec")) taskStatus = "Hoàn Thành";
            else taskStatus = "Đã Xóa";
            if(rs.getBoolean("trangthaibaocao")) taskReport = "Đã Có Báo Cáo";
            
            String tbData[] = {taskID,staffname,taskStartingDay,taskEndingDay,taskStatus,taskReport}; 
            modeltemp.addRow(tbData);
        }
    }
    public String getTaskDescription(String taskID,String projectName) throws SQLException{
        connect();
        String projectID = getProjectID(projectName);
        String query = "select noidungcongviec from congviec where sttcv = '"+taskID+"' and maduan = '"+projectID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            return rs.getString(1);
        }
        return "";
    }
    public String getTaskReport(String taskID,String projectName) throws SQLException{
        connect();
        String projectID = getProjectID(projectName);
        String query = "select noidungbaocao from CONGVIEC where sttcv = '"+taskID+"' and maduan = '"+projectID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            return rs.getString(1);
        }
        return "";
    }
    public void showStaffWithProjectID(DefaultTableModel modeltemp,String projectID) throws SQLException{ // show nhân viên vào jtable ở form phân công
        connect();
        String query = "select distinct nv.manhanvien,tennhanvien\n" +
                        "from DUAN da inner join phong p on da.maphong = p.maphong\n" +
                        "inner join THOIGIANNHANPHONG tgnp on p.maphong = tgnp.maphong\n" +
                        "inner join NHANVIEN nv on tgnp.manhanvien = nv.manhanvien\n" +
                        "where da.maduan = '"+projectID+"' and ngayroiphong is null and machucvu = 4 and trangthainhanvien = 1";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next())
        {            
            String staffID = rs.getString(1);
            String staffName = rs.getString(2);
            String tbData[] = {staffID,staffName}; 
            modeltemp.addRow(tbData);
        }
    }
    public void assignTask(ArrayList<String> assignTask) throws SQLException{
        String taskID = assignTask.get(0);
        String staffID = assignTask.get(1);
        String projectID = assignTask.get(2);
        String startingDay = assignTask.get(3);
        String endingDay = assignTask.get(4);
        connect();
        String query = "update CONGVIEC \n" +
                        "set manhanvien = '"+staffID+"', thoigianbatdaucv = '"+startingDay+"', thoigianketthuccv = '"+endingDay+"'\n" +
                        "where sttcv = '"+taskID+"' and maduan ='"+projectID+"'";
        stmt.execute(query);
        JOptionPane.showMessageDialog(null, "Giao Việc Thành Công"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
    }
    //================================================================================================
    
    //Staff===========================================================================================
    private void showTaskInFormStaffWithStaffPosition(DefaultTableModel modeltemp,String projectName) throws SQLException{
        connect();
        modeltemp.setRowCount(0); // clear jtable
        String projectID = getProjectID(projectName);
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        String query = "select * From CONGVIEC\n" +
                        "where maduan = '"+projectID+"' and manhanvien = '"+NhanVien.getInstance().getManhanvien()+"'" +
                        "        order by CASE\n" +
                        "	 WHEN  trangthaicongviec is null then 1\n" +
                        "	 WHEN trangthaicongviec = 1  THEN 2\n" +
                        "	 WHEN trangthaicongviec = 0  THEN 3\n" +
                        "        END;";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            //String staffname = "";
            String taskStartingDay = "";
            String taskEndingDay = "";
            String taskStatus = "";
            String taskReport = "Chưa Có Báo Cáo";
            
            String taskID = rs.getString("sttcv");
            //if (!StringUtils.isEmpty(rs.getString("manhanvien"))) staffname = getStaffName(rs.getString("manhanvien")); // check null
            if (!StringUtils.isEmpty(rs.getString("thoigianbatdaucv"))) taskStartingDay = date.format(rs.getDate("thoigianbatdaucv"));// check null
            if (!StringUtils.isEmpty(rs.getString("thoigianketthuccv"))) taskEndingDay = date.format(rs.getDate("thoigianketthuccv"));// check null
            if (StringUtils.isEmpty(rs.getString("trangthaicongviec"))) taskStatus = "Đang Làm";// check null
            else if(rs.getBoolean("trangthaicongviec")) taskStatus = "Hoàn Thành";
            else taskStatus = "Hủy Bỏ";
            if(rs.getBoolean("trangthaibaocao")) taskReport = "Đã gửi";
            
            String tbData[] = {taskID,taskStartingDay,taskEndingDay,taskStatus,taskReport}; 
            modeltemp.addRow(tbData);
        }
    } // show mỗi công việc của nhân viên đăng nhập
    private void showTaskInFormStaffWithOtherPosition(DefaultTableModel modeltemp,String projectName) throws SQLException{ // nếu quyền cao hơn thì show hết
        connect();
        modeltemp.setRowCount(0); // clear jtable
        String projectID = getProjectID(projectName);
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        String query = "select * From CONGVIEC\n" +
                        "where maduan = '"+projectID+"'" +
                        "        order by CASE\n" +
                        "	 WHEN  trangthaicongviec is null then 1\n" +
                        "	 WHEN trangthaicongviec = 1  THEN 2\n" +
                        "	 WHEN trangthaicongviec = 0  THEN 3\n" +
                        "        END;";
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
            //String staffname = "";
            String taskStartingDay = "";
            String taskEndingDay = "";
            String taskStatus = "";
            String taskReport = "Chưa Có Báo Cáo";
            
            String taskID = rs.getString("sttcv");
            //if (!StringUtils.isEmpty(rs.getString("manhanvien"))) staffname = getStaffName(rs.getString("manhanvien")); // check null
            if (!StringUtils.isEmpty(rs.getString("thoigianbatdaucv"))) taskStartingDay = date.format(rs.getDate("thoigianbatdaucv"));// check null
            if (!StringUtils.isEmpty(rs.getString("thoigianketthuccv"))) taskEndingDay = date.format(rs.getDate("thoigianketthuccv"));// check null
            if (StringUtils.isEmpty(rs.getString("trangthaicongviec"))) taskStatus = "Đang Làm";// check null
            else if(rs.getBoolean("trangthaicongviec")) taskStatus = "Hoàn Thành";
            else taskStatus = "Hủy Bỏ";
            if(rs.getBoolean("trangthaibaocao")) taskReport = "Đã gửi";
            
            String tbData[] = {taskID,taskStartingDay,taskEndingDay,taskStatus,taskReport}; 
            modeltemp.addRow(tbData);
        }
    }
    public void showTaskInFormStaff(DefaultTableModel modeltemp,String projectName) throws SQLException{
        if(NhanVien.getInstance().getMachucvu() == 4) showTaskInFormStaffWithStaffPosition(modeltemp, projectName);
        else showTaskInFormStaffWithOtherPosition(modeltemp, projectName);
    }
    public boolean checkReportStatus(String taskID,String projectName) throws SQLException{ // nếu có đã có report => true
        connect();
        String projectID = getProjectID(projectName);
        String query = "select trangthaibaocao from congviec where sttcv = '"+taskID+"' and maduan = '"+projectID+"'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()){
            return rs.getBoolean(1);
        }
        return false;
    }
    public void sendReport(String taskID,String projectName,String taskReport) throws SQLException{
        connect();
        String projectID = getProjectID(projectName);
        String query = "update CONGVIEC\n" +
                        "set noidungbaocao = N'"+taskReport+"', trangthaibaocao = 1\n" +
                        "where sttcv = '"+taskID+"' and maduan = '"+projectID+"'";
        stmt.execute(query);
          JOptionPane.showMessageDialog(null, "Đã Gửi Báo Cáo"," Thông Báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
    }
    //================================================================================================
    //</editor-fold>
    //==========================================================================================================
    //<editor-fold defaultstate="collapsed" desc="Restore và Back up Database">
    public void restoreDatabase(String address){
        try {
            RestoreDatabase db = new RestoreDatabase();
            db.connect();
            String query = "use master RESTORE DATABASE QuanLyNhanSu FROM DISK = '"+address+"' ";         
            db.stmt.execute(query);
            JOptionPane.showMessageDialog(null, "Restore Dữ Liệu Thành Công"," Thông báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
        } catch (SQLException ex) {
            Logger.getLogger(FormRestoreAndBackupDatabase1.class.getName()).log(Level.SEVERE, null, ex);
            if(address.equals("")) JOptionPane.showMessageDialog(null, "Vui Lòng Điền Đường Dẫn"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
            else if(!address.contains(".bak")) JOptionPane.showMessageDialog(null, "File Không Hợp Lệ"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Database đã tồn tại"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
        }
    }
    public void backupDatabase(){
        try {
            connect();
            String address = "D:\\Java\\QuanLyNhanSu\\backup\\"+java.time.LocalDate.now()+".bak";
            String query = "BACKUP DATABASE QuanLyNhanSu TO DISK = '"+address+"'";
            System.out.println(query);
            stmt.execute(query);      
            JOptionPane.showMessageDialog(null, "Backup Dữ Liệu Thành Công Vào \n"+address," Thông báo ",JOptionPane.INFORMATION_MESSAGE,icon1);
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Dữ Liệu đang Trống Không Thể Backup"," Error ",JOptionPane.INFORMATION_MESSAGE,icon2);
        }
    //</editor-fold>
    }
}

